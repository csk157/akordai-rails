# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Language.find_or_create_by_name name: 'English', code: 'EN'
Language.find_or_create_by_name name: 'Lietuviu', code: 'LT'

Type.find_or_create_by_name name: 'Chords'
Type.find_or_create_by_name name: 'Bass'
Type.find_or_create_by_name name: 'Tabs'

Chord.find_or_create_by_name name: 'G', notation: '210003'
Chord.find_or_create_by_name name: 'C', notation: 'x32010'

Genre.find_or_create_by_name name: 'Rock'
Genre.find_or_create_by_name name: 'Pop'
Genre.find_or_create_by_name name: 'Country'

Complexity.find_or_create_by_name name: 'Very Easy'
Complexity.find_or_create_by_name name: 'Easy'
Complexity.find_or_create_by_name name: 'Intermediate'
Complexity.find_or_create_by_name name: 'Hard'
Complexity.find_or_create_by_name name: 'Very Hard'