class AddNotationToChords < ActiveRecord::Migration
  def change
    add_column :chords, :notation, :string, nullable: false
  end
end
