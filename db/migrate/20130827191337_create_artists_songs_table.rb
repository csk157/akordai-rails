class CreateArtistsSongsTable < ActiveRecord::Migration
	def self.up
		create_table :artists_songs, :id => false do |t|
			t.references :artist
			t.references :song
		end
		add_index :artists_songs, [:artist_id, :song_id]
	end

	def self.down
		drop_table :artists_songs
	end
end
