class AddMoreInfoToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :wikipedia, :string
    add_column :artists, :twitter, :string
    add_column :artists, :facebook, :string
    add_column :artists, :youtube, :string
    add_column :artists, :vkontakte, :string
  end
end
