class AddSlugToChords < ActiveRecord::Migration
  def change
    add_column :chords, :slug, :string
    add_index :chords, :slug
  end
end
