class CreateChords < ActiveRecord::Migration
  def change
    create_table :chords do |t|
      t.integer :parent_id
      t.string :name

      t.timestamps
    end
  end
end
