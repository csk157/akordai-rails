class AddVersionToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :version, :integer, null: false, default: 0
  end
end
