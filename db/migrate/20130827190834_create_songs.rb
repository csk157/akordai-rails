class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.integer :user_id
      t.integer :genre_id
      t.integer :type_id
      t.integer :language_id
      t.string :name
      t.text :text
      t.string :status

      t.timestamps
    end
  end
end
