class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
      t.integer :user_id
      t.string :name
      t.text :description
      t.string :status

      t.timestamps
    end
  end
end
