class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :score_id, null: false
      t.integer :user_id
      t.string :ip, null: false
      t.boolean :up, null: false

      t.timestamps
    end
  end
end
