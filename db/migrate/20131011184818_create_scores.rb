class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.integer :up, default: 0, null: false
      t.integer :down, default: 0, null: false
      t.integer :scorable_id, null: false
      t.string :scorable_type, null: false

      t.timestamps
    end
  end
end
