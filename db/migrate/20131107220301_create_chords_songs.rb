class CreateChordsSongs < ActiveRecord::Migration
  def change
    create_table :chords_songs, :id => false do |t|
        t.references :chord
        t.references :song
    end
    add_index :chords_songs, [:chord_id, :song_id]
    add_index :chords_songs, :song_id
  end
end
