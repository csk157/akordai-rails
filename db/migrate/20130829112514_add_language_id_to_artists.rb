class AddLanguageIdToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :language_id, :integer, nullable: false
  end
end
