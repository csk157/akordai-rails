class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :artists, :user_id
    add_index :artists, :language_id
    add_index :chords, :parent_id
    add_index :songs, :user_id
    add_index :songs, :genre_id
    add_index :songs, :type_id
    add_index :songs, :language_id
    add_index :songs, :complexity_id
    add_index :votes, :score_id
    add_index :votes, :user_id
    add_index :scores, [:scorable_id, :scorable_type]
  end
end
