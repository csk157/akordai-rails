source 'https://rubygems.org'

ruby '2.0.0'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'therubyracer'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
	# bundle exec rake doc:rails generates the API under doc/api.
	gem 'sdoc', require: false
end

gem 'sitemap_generator'
gem 'bootstrap-sass', '~> 3.0.3.0'
gem 'font-awesome-rails'
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'
# gem 'acts-as-taggable-on'
gem 'simple_form', github: 'plataformatec/simple_form'
gem 'friendly_id', github: 'norman/friendly_id'

gem "paperclip", :git => "git://github.com/thoughtbot/paperclip.git"
gem "breadcrumbs_on_rails"
gem 'jquery-ui-rails'
gem 'forem', :github => "radar/forem", :branch => "rails4"
gem 'cancan', git: "https://github.com/nukturnal/cancan.git"
gem 'will_paginate'
gem 'will_paginate-bootstrap'

group :production do
  gem 'rails_12factor'
  gem "unicorn"
end
# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
# gem 'sunspot_rails'
# gem 'sunspot_solr' # optional pre-packaged Solr distribution for use in development

group :development do
  gem "faker"
  gem 'factory_girl_rails'
  gem "rspec-nc"
  gem "guard"
  gem "guard-bundler"
  gem "guard-cucumber"
  gem "guard-rspec"
  gem "guard-zeus"
  gem "rspec-rails"
  gem "rb-inotify", require: false
  gem "rb-fsevent", require: false
  gem "rb-fchange", require: false
  gem 'meta_request'
  gem 'bullet'
  gem 'ruby-growl'
  gem 'quiet_assets'
end

group :test do
  gem "capybara"
  gem "rspec-rails"
  gem "cucumber-rails", require: false
  gem 'factory_girl_rails'
  gem 'database_cleaner', '< 1.1.0'
  gem 'simplecov', :require => false
end