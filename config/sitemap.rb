# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.example.com"

SitemapGenerator::Sitemap.create do
  forem = Forem::Engine::routes.url_helpers
  add forem_path, :changefreq => 'daily'
  
  "#abcdefghijklmnopqrstuvwxyz".each_char do |l|
    add letter_path(l), :changefreq => 'daily'
  end

  add chords_path, :changefreq => 'weekly'
  Chord.all.each do |c|
    add chord_path(c), :lastmod => c.updated_at
  end

  add artists_path, :changefreq => 'weekly'
  Artist.all_published.each do |a|
    add artist_path(a), :lastmod => a.updated_at
  end

  Song.all_published.each do |s|
    add song_path(s), :lastmod => s.updated_at
  end

  User.all.each do |u|
    add user_path(u)
  end

  Forem::Engine::Category.all.each do |c|
    add forem.category_path(c), :lastmod => c.updated_at
  end

  Forem::Engine::Forum.all.each do |f|
    add forem.forum_path(f), :changefreq => 'daily'
    f.topics.each do |t|
      add forem.forum_topic_path(f, t), :lastmod => t.updated_at
    end
  end 

  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end
