AkordaiRails::Application.routes.draw do

  resources :bookmarks do
    get "(/:type)", action: 'index', as: '', on: :collection
    delete "", action: 'destroy', as: :delete, on: :collection
  end

  # This line mounts Forem's routes at /forums by default.
  # This means, any requests to the /forums URL of your application will go to Forem::ForumsController#index.
  # If you would like to change where this extension is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Forem relies on it being the default of "forem"
  mount Forem::Engine, :at => '/forums'

  resources :complexities

  post "scores/:id/vote", controller: 'scores', action: 'vote'
  get "scores/:id/voted", controller: 'scores', action: 'voted'
  get "letters/:letter", controller: 'letters', action: 'index', as: :letter
  get "search(/:q)", controller: 'searches', action: 'search', as: :search

  resources :genres, except: [:show]
  resources :languages, except: [:show]
  resources :types, except: [:show]
  resources :chords do
    get 'search(/:q)', action: :search, on: :collection
  end
  
  resources :artists do
    get 'unpublished', action: :unpublished, on: :collection
    put 'publish', action: :publish
    put 'unpublish', action: :unpublish
    get 'search(/:q)', action: :search, on: :collection
  end

  resources :songs, except: [:index] do
    get 'unpublished', action: :unpublished, on: :collection
    put 'publish', action: :publish
    put 'unpublish', action: :unpublish
    get 'search(/:q)', action: :search, on: :collection
  end
  get "search(/:query)", controller: :searches, action: :search

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations" }
  resources :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#home'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
