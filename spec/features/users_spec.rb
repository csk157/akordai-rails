require 'spec_helper'
include Warden::Test::Helpers
Warden.test_mode!
describe 'User logins' do
  let(:password) { 'foobarasdasd' }
  let(:user) { FactoryGirl.create(:user, password: password, password_confirmation: password) }

  before(:each) { user.confirm! }

  it "should be able to login with valid credentials" do
    visit '/'
    within("#new_user") { login_with user.email, password }

    expect(page).to have_content(I18n.t('user_menu.logout'))
    expect(current_path).to eq root_path
  end

  it "should be redirected without valid credentials" do
    visit '/'
    within("#new_user") { login_with 'invalid@email.com', password }

    expect(page).to have_content(I18n.t('user.signin_page.title'))
    expect(current_path).to eq new_user_session_path
  end

  it "should be able to logout" do
    login_as user, scope: :user
    visit '/'
    click_link I18n.t('user_menu.logout')

    expect(page).to have_content(I18n.t('user.signin'))
  end
end

def login_with(email, pass)
  fill_in 'user_email', with: email
  fill_in 'user_password', with: pass
  click_button I18n.t('user.login')
end

describe 'User registers' do
  let(:password) { 'foobarasdasd' }
  let(:name) { 'helou uou' }
  let(:email) { 'fafa@lala.com' }

  it "should be able to register with valid info" do
    visit new_user_registration_path
    register_with(name, email, password)

    expect(current_path).to eq root_path
  end

  it "should not be able to register without valid info" do
    visit new_user_registration_path
    register_with('', email, password)

    expect(page).to have_content(I18n.t('user.signup_page.title'))
  end

end

def register_with(name, email, pass)
  within(".new-registration-form") do
    fill_in 'user_name', with: name
    fill_in 'user_email', with: email
    fill_in 'user_password', with: pass
    fill_in 'user_password_confirmation', with: pass
    click_button I18n.t('user.signup_page.submit_button')
  end
end


describe "User edits profile" do
  let(:user) do
    u = FactoryGirl.create(:user, password: 'superpass', password_confirmation: 'superpass')
    u.confirm!
    return u
  end

  before(:each) { login_as user, scope: :user }

  it "should be able to change password if both passwords are identical" do
    pass = 'somethingNew123'
    visit edit_user_registration_path(user)
    within("#edit_user") do
      fill_in 'user_password', with: pass
      fill_in 'user_password_confirmation', with: pass
      click_button I18n.t('user.edit_page.submit_button')
    end
    expect(current_path).to eq user_path(user)
  end

  it "should not be able to change password if passwords differ" do
    pass = 'somethingNew123'
    visit edit_user_registration_path(user)
    within("#edit_user") do
      fill_in 'user_password', with: pass
      fill_in 'user_password_confirmation', with: "not1equeal"
      click_button I18n.t('user.edit_page.submit_button')
    end
    expect(current_path).not_to eq user_path(user)
  end
end

describe "Seeing user profile" do
  let(:user) do
    u = FactoryGirl.create(:user, password: 'superpass', password_confirmation: 'superpass')
    u.confirm!
    return u
  end

  it "should show user profile" do
    visit user_path(user)
    expect(page).to have_content(user.name)
  end

  it "should show edit link if viewing own profile" do
    login_as user, scope: :user
    visit user_path(user)
    expect(page).to have_content(I18n.t 'user.user_edit')
  end
end