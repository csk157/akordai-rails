require 'spec_helper'

describe Vote do
  describe "Validations" do
    let(:vote) { FactoryGirl::build(:vote, user: nil) }

    it "Should be invalid without score" do
      vote.score_id = nil
      expect(vote.valid?).to be_false
    end

    it "Should be invalid with non existent score_id" do
      vote.score_id = 0
      expect(vote.valid?).to be_false
    end

    it "Should be valid with blank user" do
      vote.user_id = nil
      expect(vote.valid?).to be_true
    end

    it "Should not be valid with non existing user_id" do
      vote.user = nil
      vote.user_id = 0
      expect(vote.valid?).to be_false
    end

    it "Should not allow duplicate user_id for the same score" do
      v = FactoryGirl::create(:vote)
      vote.user = v.user
      vote.ip = "78.143.92.131"
      vote.score = v.score
      expect(vote.valid?).to be_false
    end

    it "Should not be valid without ip" do
      vote.ip = nil
      expect(vote.valid?).to be_false

      vote.ip = ''
      expect(vote.valid?).to be_false
    end

    it "Should not be valid with faulty ip address" do
      vote.ip = '266.244.12.23'
      expect(vote.valid?).to be_false
    end

    it "Should not allow duplicate ip for the same score" do
      v = FactoryGirl::create(:vote, user: nil)
      vote.ip = v.ip
      vote.score = v.score
      expect(vote.valid?).to be_false
    end
  end

  it "Increments score after creation" do
    score = FactoryGirl::create(:score)

    FactoryGirl::create(:vote, up: true, score: score)
    expect(score.up).to eq(1)

    FactoryGirl::create(:vote, up: false, score: score, ip: '78.143.92.131')
    expect(score.down).to eq(1)
  end

  it "Correctly changes score after changed mind" do
    score = FactoryGirl::create(:score)

    vote = FactoryGirl::create(:vote, up: true, score: score)
    ups = score.up
    downs = score.down

    expect(vote.change_to false).to be_true
    score.reload

    expect(score.up).to eq(ups - 1)
    expect(score.down).to eq(downs + 1)
  end
end
