require 'spec_helper'

describe "Artist" do
	describe "Validations" do
		let(:artist) { artist = FactoryGirl::build(:artist) }

		it "Should not allow to save artist without name" do
			artist.name = nil
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow with blank name" do
			artist.name = ''
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow to save artist without language" do
			artist.language_id = nil
			expect(artist.valid?).not_to be_true
		end

		it "should not allow to save artist with non existing language" do
			artist.language_id = 0
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow to save artist without user" do
			artist.user_id = nil
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow to save artist with non existing user_id" do
			artist.user_id = 0
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow not predifined statuses" do
			artist.status = "nonesense"
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow invalid wiki links" do
			artist.wikipedia = 'htwik.pedi.com/stuff'
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow invalid facebook links" do
			artist.facebook = 'htwik.pedi.com/stuff'
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow invalid youtube links" do
			artist.youtube = 'htwik.pedi.com/stuff'
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow invalid twitter links" do
			artist.twitter = 'htwik.pedi.com/stuff'
			expect(artist.valid?).not_to be_true
		end

		it "Should not allow invalid vkontakte links" do
			artist.vkontakte = 'htwik.pedi.com/stuff'
			expect(artist.valid?).not_to be_true
		end
	end

	it "Should create score after creation" do
		artist = FactoryGirl.create(:artist)
		expect(artist.score).not_to be_nil
	end

	it "Should return all published artists" do
		a1 = FactoryGirl.create(:artist, status: 'unpublished')
		a2 = FactoryGirl.create(:artist, status: 'published')
		a3 = FactoryGirl.create(:artist, status: 'published')

		expect(Artist.all_published.to_set).to eq [a2,a3].to_set
	end

	it "Should return all unpublished artists" do
		a1 = FactoryGirl.create(:artist, status: 'unpublished')
		a2 = FactoryGirl.create(:artist, status: 'unpublished')
		a3 = FactoryGirl.create(:artist, status: 'published')

		expect(Artist.all_unpublished.to_set).to eq [a1,a2].to_set
	end

	it "Should return artists starting with specific letter" do
		a1 = FactoryGirl.create(:artist, name: 'Jonas')
		a2 = FactoryGirl.create(:artist, name: 'jack')
		a3 = FactoryGirl.create(:artist, name: 'julie')

		a4 = FactoryGirl.create(:artist, name: 'Dan')
		a5 = FactoryGirl.create(:artist, name: 'Dully')

		a6 = FactoryGirl.create(:artist, name: 'Guliber')

		an1 = FactoryGirl.create(:artist, name: '9 unu')
		an2 = FactoryGirl.create(:artist, name: '19 unu')
		an3 = FactoryGirl.create(:artist, name: '# lal')
		an4 = FactoryGirl.create(:artist, name: '@lay')
		an5 = FactoryGirl.create(:artist, name: 'šūšū')

		Artist.find_by_letter('J').should =~ [a1,a2,a3]
		Artist.find_by_letter('d').should =~ [a4,a5]
		Artist.find_by_letter('g').should =~ [a6]
		Artist.find_by_letter('s').should =~ []
		Artist.find_by_letter('#').should =~ [an1,an2,an3,an4,an5]
		Artist.find_by_letter('').should =~ (Artist.all)
	end

	it "publishes artist" do
		s = FactoryGirl.create(:artist, status: 'unpublished')
		s.publish
		expect(s.status).to eq 'published'
	end

	it "unpublishes artist" do
		s = FactoryGirl.create(:artist, status: 'published')
		s.unpublish
		expect(s.status).to eq 'unpublished'
	end
end
