require 'spec_helper'

describe "Chord" do
	describe "validations" do
		let(:chord) { FactoryGirl::build(:chord) }
		it "Should have name" do
			chord.name = nil
			expect(chord.valid?).not_to be_true
		end

		it "should have non blank name" do
			chord.name = ''
			expect(chord.valid?).not_to be_true
		end

		it "Should have notation" do
			chord.notation = ''
			expect(chord.valid?).not_to be_true
		end

		it "Should allow valid notation" do
			chord.notation = 'x01010'
			expect(chord.valid?).to be_true
		end

		it "Should not allow invalid notation" do
			chord.notation = 'x080h0'
			expect(chord.valid?).not_to be_true
		end

		it "Should not allow nonexisting parent id" do
			chord.parent_id = 0
			expect(chord.valid?).not_to be_true
		end

		it "Should not allow non unique name" do
			FactoryGirl::create(:chord, name: 'D#')
			chord.name = 'D#'
			expect(chord.valid?).not_to be_true
		end

		it "should reassociate songs on save" do
			s = FactoryGirl::create(:song, text: "[A]         [G]\nHellou yo yo man man B wit m E f\n[F#]     D")
			s2 = FactoryGirl::create(:song, text: "[G]         [G]\nHellou yo yo man man B wit m E f\n[F#]     D")

			c = FactoryGirl::create(:chord, name: 'G')
			expect(c.songs.to_set).to eq [s, s2].to_set

			c.name = "A"
			c.save
			expect(c.songs.to_set).to eq [s].to_set
		end

		it "Should destroy children when removed" do
			chord = FactoryGirl::create(:chord, name: 'D')

			chord1 = FactoryGirl::create(:chord, name: 'D2')
			chord2 = FactoryGirl::create(:chord, name: 'D3')
			chord.children << chord1
			chord.children << chord2
			chord.destroy

			expect(Chord.exists?(chord1.id) && !Chord.exists?(chord2.id)).not_to be_true
		end
	end
end
