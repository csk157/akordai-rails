require 'spec_helper'

describe Score do
  describe "Validations" do
    let (:score) { FactoryGirl::build(:score) }
    it "Should not be valid without scorable" do
      score.scorable_id = nil
      score.scorable_type = nil
      expect(score.valid?).to be_false
    end

    it "Should not be valid with non existing scorable" do
      score.scorable_id = 0
      score.scorable_type = 'Artist'
      expect(score.valid?).to be_false
    end
  end

  describe "find_vote" do
    let (:score) { FactoryGirl::create(:artist).score }

    it "should return null if no vote found" do
      expect(score.find_vote(nil, "192.168.0.1")).to be_nil
    end

    it "should return vote if vote found" do
      score.vote_with_results(nil, "192.168.0.1", 'true')
      expect(score.find_vote(nil, "192.168.0.1")).not_to be_nil
    end
  end

  describe "vote_with_results" do
    let (:score) { FactoryGirl::create(:artist).score }

    it "should create vote" do
      res = score.vote_with_results(nil, "192.168.0.1", 'true')
      expect(score.votes.count).to eq 1
      expect(res[:success]).to be_true 
      expect(res[:score]).to eq 1 
    end

    it "should change vote down" do
      score.vote_with_results(nil, "192.168.0.1", 'true')
      res = score.vote_with_results(nil, "192.168.0.1", 'false')

      expect(score.votes.count).to eq 1
      expect(res[:success]).to be_true 
      expect(res[:score]).to eq(-1)
    end

    it "should change vote up" do
      score.vote_with_results(nil, "192.168.0.1", 'false')
      res = score.vote_with_results(nil, "192.168.0.1", 'true')

      expect(score.votes.count).to eq 1
      expect(res[:success]).to be_true 
      expect(res[:score]).to eq 1
    end
  end
end