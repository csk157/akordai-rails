require 'spec_helper'

describe "Type" do

	describe "validations" do
		let(:type) { FactoryGirl::build(:type) }

		it "should have name" do
			type.name = nil
			expect(type.valid?).to be_false
		end

		it "should have non blank name" do
			type.name = ''
			expect(type.valid?).to be_false
		end

		it "should have unique name" do
			FactoryGirl::create(:type, name: 'Name')
			type.name = "Name"
			expect(type.valid?).to be_false
		end
	end
end
