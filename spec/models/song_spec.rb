require 'spec_helper'

describe "Song" do
	describe "validations" do
		let(:song) { FactoryGirl::build(:song) }

		it "should have name" do
			song.name = nil
			expect(song.valid?).to be_false
		end

		it "should have non blank name" do
			song.name = ''
			expect(song.valid?).to be_false
		end

		it "should have text" do
			song.text = nil
			expect(song.valid?).to be_false
		end

		it "should have non blank text" do
			song.text = ''
			expect(song.valid?).to be_false
		end

		it "should not allow not predefined statuses" do
			song.status = "nonesense"
			expect(song.valid?).to be_false
		end

		it "should have user" do
			song.user_id = nil
			expect(song.valid?).to be_false
		end

		it "should have valid user_id" do
			song.user_id = 0
			expect(song.valid?).to be_false
		end

		it "shoud have language" do
			song.language_id = nil
			expect(song.valid?).to be_false
		end

		it "should have valid language_id" do
			song.language_id = 0
			expect(song.valid?).to be_false
		end

		it "shoud have genre" do
			song.genre_id = nil
			expect(song.valid?).to be_false
		end

		it "should have valid genre_id" do
			song.genre_id = 0
			expect(song.valid?).to be_false
		end

		it "shoud have type" do
			song.type_id = nil
			expect(song.valid?).to be_false
		end

		it "should have valid type_id" do
			song.type_id = 0
			expect(song.valid?).to be_false
		end

		it "should have complexity" do
			song.complexity_id = nil
			expect(song.valid?).to be_false
		end

		it "should have valid complexity_id" do
			song.complexity_id = 0
			expect(song.valid?).to be_false
		end
	end

	it "should correctly tell if song has chord" do
		s = FactoryGirl::build(:song)
		s.text = "[A]         [G]\nHellou yo yo man man B wit m E f\n[F#]     D"

		expect(s.has_chord?("A")).to be_true
		expect(s.has_chord?("G")).to be_true
		expect(s.has_chord?("F#")).to be_true
		expect(s.has_chord?("B")).to be_false
		expect(s.has_chord?("E")).to be_false
		expect(s.has_chord?("D")).to be_false
	end

	it "should reassociate chords on save" do
		c1 = FactoryGirl::create(:chord, name: 'G')
		c2 = FactoryGirl::create(:chord, name: 'A')
		c3 = FactoryGirl::create(:chord, name: 'F#')
		c4 = FactoryGirl::create(:chord, name: 'E')

		s = FactoryGirl::create(:song, text: "[A]         [G]\nHellou yo yo man man B wit m E f\n[F#]     D")
		expect(s.chords.to_set).to eq [c2, c1, c3].to_set
	end

	it "should parse artist tokens correctly" do
		a1 = FactoryGirl::create(:artist)
		a2 = FactoryGirl::create(:artist)

		song = FactoryGirl::build(:song, artist_tokens: "#{a1.id},#{a2.id}")
		expect(song.artist_ids).to eq([a1.id, a2.id])
	end

	it "Should create score association after create" do
		song = FactoryGirl::create(:song)
		expect(song.score).not_to be_nil
	end

	it "Should assign right version to item if it has same name and artists" do
		a1 = FactoryGirl::create(:artist)
		a2 = FactoryGirl::create(:artist)

		song = FactoryGirl::create(:song, name: 'Something same', artist_tokens: "#{a1.id},#{a2.id}")
		expect(song.version).to eq(0)

		song = FactoryGirl::create(:song, name: 'Something same', artist_tokens: "#{a1.id},#{a2.id}")
		expect(song.version).to eq(1)
	end

	it "Should not increase version if artists are not the same" do
		a1 = FactoryGirl::create(:artist)
		a2 = FactoryGirl::create(:artist)

		FactoryGirl::create(:song, name: 'Something same', artist_tokens: "#{a2.id}")
		song = FactoryGirl::create(:song, name: 'Something same', artist_tokens: "#{a1.id},#{a2.id}")
		expect(song.version).to eq(0)
	end

	it "Should fix versions after deletion" do
		a1 = FactoryGirl::create(:artist)
		a2 = FactoryGirl::create(:artist)

		song1 = FactoryGirl::create(:song, name: 'Something same', artist_tokens: "#{a1.id},#{a2.id}")
		song2 = FactoryGirl::create(:song, name: 'Something same', artist_tokens: "#{a1.id},#{a2.id}")

		song1.destroy
		song2.reload
		expect(song2.version).to eq(0)
	end

	it "Should return songs starting with specific letter" do
		s1 = FactoryGirl.create(:song, name: "July")
		s2 = FactoryGirl.create(:song, name: "Joy")
		s3 = FactoryGirl.create(:song, name: "January")

		s4 = FactoryGirl.create(:song, name: "Fly")
		s5 = FactoryGirl.create(:song, name: "Flu")

		s6 = FactoryGirl.create(:song, name: "My man")

		sn1 = FactoryGirl.create(:song, name: "3 Utiutiu")
		sn2 = FactoryGirl.create(:song, name: "^$ fafa")
		sn3 = FactoryGirl.create(:song, name: "# asdas")

		expect(Song.find_by_letter('j').to_set).to eq([s1,s2,s3].to_set)
		expect(Song.find_by_letter('f').to_set).to eq([s4,s5].to_set)
		expect(Song.find_by_letter('m').to_set).to eq([s6].to_set)
		expect(Song.find_by_letter('a').to_set).to eq([].to_set)
		expect(Song.find_by_letter('#').to_set).to eq([sn1, sn2, sn3].to_set)
		expect(Song.find_by_letter('').to_set).to eq(Song.all.to_set)
	end

	it "returns only published" do
		s1 = FactoryGirl.create(:song, status: "unpublished")
		s2 = FactoryGirl.create(:song, status: "published")
		s3 = FactoryGirl.create(:song, status: "unpublished")

		expect(Song.all_published.to_set).to eq [s2].to_set
	end

	it "returns unpublished" do
		s1 = FactoryGirl.create(:song, status: "unpublished")
		s2 = FactoryGirl.create(:song, status: "published")
		s3 = FactoryGirl.create(:song, status: "unpublished")

		expect(Song.all_unpublished.to_set).to eq [s1,s3].to_set
	end

	it "publishes song" do
		s = FactoryGirl.create(:song, status: 'unpublished')
		s.publish
		expect(s.status).to eq 'published'
	end

	it "unpublishes song" do
		s = FactoryGirl.create(:song, status: 'published')
		s.unpublish
		expect(s.status).to eq 'unpublished'
	end
end
