require 'spec_helper'

describe Complexity do
  describe "validations" do
    let(:complexity) { FactoryGirl::build(:complexity) }
    it "Should have name" do
      complexity.name = nil
      expect(complexity.valid?).not_to be_true
    end

    it "should have non blank name" do
      complexity.name = ''
      expect(complexity.valid?).not_to be_true
    end
  end
end
