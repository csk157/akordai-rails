require 'spec_helper'

describe Bookmark do
  describe "Validations" do
  	it "should not be valid without existing bookmarkable" do
  		u = FactoryGirl::create(:user)
  		b = FactoryGirl::build(:bookmark, user_id: u.id, bookmarkable_type: 'Artist', bookmarkable_id: 2)

  		expect(b.valid?).not_to be_true
  	end

  	it "should not be valid without existing user" do
  		a = FactoryGirl::create(:artist)
  		b = FactoryGirl::build(:bookmark, user_id: 999, bookmarkable_type: 'Artist', bookmarkable_id: a.id)

  		expect(b.valid?).not_to be_true
  	end

  	it "should not create duplicates" do
  		u = FactoryGirl::create(:user)
  		a = FactoryGirl::create(:artist)

  		FactoryGirl::create(:bookmark, user_id: u.id, bookmarkable_type: 'Artist', bookmarkable_id: a.id)
  		b = FactoryGirl::build(:bookmark, user_id: u.id, bookmarkable_type: 'Artist', bookmarkable_id: a.id)
  		expect(b.valid?).not_to be_true
  	end
  end

  it "should tell if bookmark exists" do
    u = FactoryGirl::create(:user)
    a = FactoryGirl::create(:artist)
    a2 = FactoryGirl::create(:artist)
    FactoryGirl::create(:bookmark, user_id: u.id, bookmarkable_type: 'Artist', bookmarkable_id: a.id)

    expect(Bookmark.bookmarked?(u, a)).to be_true
    expect(Bookmark.bookmarked?(u, a2)).to be_false
  end

  describe "Returns" do
  	let(:user) { FactoryGirl::create(:user) }  	
  	let(:artist1) { FactoryGirl::create(:artist) }  	
  	let(:artist2) { FactoryGirl::create(:artist) }  	
  	let(:song1) { FactoryGirl::create(:song) }  	
  	let(:song2) { FactoryGirl::create(:song) }  	
  	let(:chord1) { FactoryGirl::create(:chord) }  	
  	let(:chord2) { FactoryGirl::create(:chord) }  	
  	let(:user1) { FactoryGirl::create(:user) }  	
  	let(:user2) { FactoryGirl::create(:user) }  	
  	
  	
  	let(:artist_bookmark) { FactoryGirl::create(:bookmark, user_id: user.id, bookmarkable_type: 'Artist', bookmarkable_id: artist1.id) }
  	let(:song_bookmark) { FactoryGirl::create(:bookmark, user_id: user.id, bookmarkable_type: 'Song', bookmarkable_id: song1.id) }
  	let(:chord_bookmark) { FactoryGirl::create(:bookmark, user_id: user.id, bookmarkable_type: 'Chord', bookmarkable_id: chord1.id) }
  	let(:user_bookmark) { FactoryGirl::create(:bookmark, user_id: user.id, bookmarkable_type: 'User', bookmarkable_id: user1.id) }


  	it "should return only bookmarked artists" do
  		expect(Bookmark.artists(user.id)).to eq [artist_bookmark]
  	end

  	it "should return only bookmarked songs" do
  		expect(Bookmark.songs(user.id)).to eq [song_bookmark]
  	end

  	it "should return only bookmarked chords" do
  		expect(Bookmark.chords(user.id)).to eq [chord_bookmark]
  	end

  	it "should return only bookmarked users" do
  		expect(Bookmark.users(user.id)).to eq [user_bookmark]
  	end
  end
  
end
