require 'spec_helper'

describe "Genre" do
	describe "validations" do
		let(:genre) { FactoryGirl::build(:genre) }

		it "should have name" do
			genre.name = nil
			expect(genre.valid?).not_to be_true
		end

		it "should have non blank name" do
			genre.name = ''
			expect(genre.valid?).not_to be_true
		end

		it "should have unique name" do
			FactoryGirl::create(:genre, name: 'Rock')

			genre.name = 'Rock'
			expect(genre.valid?).not_to be_true
		end
	end
end
