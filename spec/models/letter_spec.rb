require 'spec_helper'

describe Letter do
	it "should create letter" do
		letter = Letter.new('a')
		letter.should_not be_nil
	end

	it "should get artists starting with letter" do
		a1 = FactoryGirl.create(:artist, name: "Lola")
		a2 = FactoryGirl.create(:artist, name: "Lila")
		a3 = FactoryGirl.create(:artist, name: "Lloula")
		a4 = FactoryGirl.create(:artist, name: "ooooLloula")

		letter = Letter.new('l')
		letter.fill

		expect(letter.artists.to_set).to eq([a2,a3,a1].to_set) # Artists ordered in alphabetic order
	end

	it "should get songs starting with letter" do
		a1 = FactoryGirl.create(:song, name: "Lola")
		a2 = FactoryGirl.create(:song, name: "Lila")
		a3 = FactoryGirl.create(:song, name: "Lloula")
		a4 = FactoryGirl.create(:song, name: "ooooLloula")

		letter = Letter.new('l')
		letter.fill

		expect(letter.songs.to_set).to eq([a1,a2,a3].to_set)
	end
end

