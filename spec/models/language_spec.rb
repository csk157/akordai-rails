require 'spec_helper'

describe "Language" do
	describe "validations" do
		let(:language) { FactoryGirl::build(:language) }

		it "should have name" do
			language.name = nil
			expect(language.valid?).to be_false
		end

		it "should have non blank name" do
			language.name = ''
			expect(language.valid?).to be_false
		end

		it "shud have unique name" do
			FactoryGirl::create(:language, name: "Russian")
			language.name = 'Russian'
			expect(language.valid?).to be_false
		end

		it "should have code" do
			language.code = nil
			expect(language.valid?).to be_false
		end

		it "should have none blank code" do
			language.code = ''
			expect(language.valid?).to be_false
		end

		it "shoul have unique code" do
			FactoryGirl::create(:language, code: "Ru")
			language.code = 'Ru'
			expect(language.valid?).to be_false
		end
	end
end
