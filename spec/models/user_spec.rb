require 'spec_helper'

describe "User", ActiveSupport::TestCase do
  describe "validations" do
    let(:user) { FactoryGirl::build(:user) }

    it "should have name" do
      user.name = nil
      expect(user.valid?).to be_false
    end

    it "should have non blank name" do
      user.name = ''
      expect(user.valid?).to be_false
    end
  end

  it "should tell if user bookmarked given item" do
    u = FactoryGirl::create(:user)
    a = FactoryGirl::create(:artist)
    a2 = FactoryGirl::create(:artist)
    FactoryGirl::create(:bookmark, user_id: u.id, bookmarkable_type: 'Artist', bookmarkable_id: a.id)

    expect(u.bookmarked?(a)).to be_true
    expect(u.bookmarked?(a2)).to be_false
  end
end
