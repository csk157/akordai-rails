require 'spec_helper'

describe LanguagesController do
  describe "new" do
    it "redirects user to homepage" do
      sign_in
      get :new
      expect(response).to redirect_to root_path
    end

    context "when not logged in" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    it "assigns new song for admin" do
      sign_in FactoryGirl::build_stubbed(:user), true
      get :new
      expect(assigns(:language)).to be_new_record
    end
  end

  describe "create" do
    context "with valid parameters" do
      let(:language) { FactoryGirl::build(:language).attributes }

      context "logged in as user" do
        before(:each) { sign_in }

        it "redirects to login page" do
          post :create, language
          expect(response).to redirect_to root_path
        end
      end

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "creates language" do
          post :create, language: language
          expect(Language.all.count).to eq 1
        end

        it "redirects to new language" do
          post :create, language: language
          expect(response).to redirect_to language_path(Language.all.last)
        end
      end

      context "when not logged in" do
        it "redirects to login page" do
          post :create, language
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    context "with invalid parameters" do
      let(:language) { FactoryGirl::attributes_for(:language, name: '') }

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "shows new page with unsaved model" do
          post :create, language: language
          expect(response).to render_template('new')
          expect(assigns(:language)).to be_new_record
        end
      end
    end
  end

  describe "update" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:language)

      put :update, id: c.to_param, language: {name: 'newname'}
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:language)

      put :update, id: c.to_param, language: {name: 'newname'}
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:language) { FactoryGirl::create(:language) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      context "with valid parameters" do
        it "redirects to language" do
          put :update, id: language.to_param, language: { name: 'newname' }
          expect(response).to redirect_to(language_path(language))
        end

        it "assings language" do
          put :update, id: language.to_param, language: { name: 'newname' }
          expect(assigns(:language).name).to eq 'newname'
        end

      end

      context "with invalid parameters" do
        it "shows edit template" do
          put :update, id: language.to_param, language: { name: '' }
          expect(response).to render_template('edit')
        end
      end
    end
  end

  describe "edit" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:language)

      get :edit, id: c.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:language)

      get :edit, id: c.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:language) { FactoryGirl::create(:language) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
      it "assigns language" do
        get :edit, id: language.to_param
        expect(assigns(:language)).to eq language
      end
    end
  end

  describe "destroy" do
    it "redirects non admin" do
      sign_in
      s = FactoryGirl::create(:language)

      delete :destroy, id: s.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      s = FactoryGirl::create(:language)
      delete :destroy, id: s.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:language) { FactoryGirl::create(:language) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      it "destroys language" do
        delete :destroy, id: language.to_param
        expect(Language.count).to eq 0
      end

      it "redirects to artists path" do
        delete :destroy, id: language.to_param
        expect(response).to redirect_to(languages_path)
      end
    end
  end
end