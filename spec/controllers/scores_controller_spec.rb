require 'spec_helper'

describe ScoresController do
  let(:score) { FactoryGirl::create(:artist).score }  
  describe "POST #vote" do
    it "not logged in can vote" do
      post :vote, id: score.id, up: true, format: :json
      expect(response).to be_ok
    end

    it "logged in can vote" do
      sign_in
      post :vote, id: score.id, up: true, format: :json

      expect(response).to be_ok
    end
  end

  describe "GET #voted" do
    it "not logged in can get vote" do
      get :voted, id: score.id, format: :json
      expect(response).to be_ok
    end

    it "logged in can get vote" do
      sign_in
      get :voted, id: score.id, format: :json

      expect(response).to be_ok
    end
  end
end
