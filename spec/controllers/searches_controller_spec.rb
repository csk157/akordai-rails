require 'spec_helper'
describe SearchesController do
  it "assings search" do
    get :search, q: 'something'
    expect(assigns(:search)).to be_a(Search)
  end
end
