require 'spec_helper'

describe BookmarksController do
  describe "POST create" do
    it "should redirect not logged in users" do
      post :create, type: 'artist', id: 1
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe "DELETE destroy" do
    it "should redirect not logged in users" do
      delete :destroy, {bookmarkable_type: 'Artist', bookmarkable_id: 1}
      expect(response).to redirect_to new_user_session_path
    end

    it "should delete bookmark with given type and id" do
      u = FactoryGirl::create(:user)
      a = FactoryGirl::create(:artist)
      FactoryGirl::create(:bookmark, user_id: u.id, bookmarkable_type: 'Artist', bookmarkable_id: a.id)
      sign_in u
      delete :destroy, {bookmarkable_type: 'Artist', bookmarkable_id: a.id}

      expect(Bookmark.count).to eq 0
    end
  end

  describe "GET index" do
    describe "logged in" do
      before(:each) do
        sign_in FactoryGirl::build_stubbed(:user)
        Bookmark.stub(:artists).and_return([])
        Bookmark.stub(:songs).and_return([])
        Bookmark.stub(:chords).and_return([])
        Bookmark.stub(:users).and_return([])
      end

      it "shows only current user's bookmarks" do
        u = FactoryGirl::build_stubbed(:user)
        sign_in u

        get :index, type: 'artists'
        expect(Bookmark).to have_received(:artists).with(u.id)
      end

      it "shows only songs" do
        get :index, type: 'songs'
        expect(Bookmark).to have_received(:songs)
      end

      it "shows only artists" do
        get :index, type: 'artists'
        expect(Bookmark).to have_received(:artists)
      end

      it "shows only chords" do
        get :index, type: 'chords'
        expect(Bookmark).to have_received(:chords)
      end

      it "shows only users" do
        get :index, type: 'users'
        expect(Bookmark).to have_received(:users)
      end
    end

    describe "not logged in" do
      it "redirects to login path" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end
  end
end
