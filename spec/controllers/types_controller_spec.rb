require 'spec_helper'

describe TypesController do
  describe "new" do
    it "redirects user to homepage" do
      sign_in
      get :new
      expect(response).to redirect_to root_path
    end

    context "when not logged in" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    it "assigns new song for admin" do
      sign_in FactoryGirl::build_stubbed(:user), true
      get :new
      expect(assigns(:type)).to be_new_record
    end
  end

  describe "create" do
    context "with valid parameters" do
      let(:type) { FactoryGirl::build(:type).attributes }

      context "logged in as user" do
        before(:each) { sign_in }

        it "redirects to login page" do
          post :create, type
          expect(response).to redirect_to root_path
        end
      end

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "creates type" do
          post :create, type: type
          expect(Type.all.count).to eq 1
        end

        it "redirects to new type" do
          post :create, type: type
          expect(response).to redirect_to type_path(Type.all.last)
        end
      end

      context "when not logged in" do
        it "redirects to login page" do
          post :create, type
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    context "with invalid parameters" do
      let(:type) { FactoryGirl::attributes_for(:type, name: '') }

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "shows new page with unsaved model" do
          post :create, type: type
          expect(response).to render_template('new')
          expect(assigns(:type)).to be_new_record
        end
      end
    end
  end

  describe "update" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:type)

      put :update, id: c.to_param, type: {name: 'newname'}
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:type)

      put :update, id: c.to_param, type: {name: 'newname'}
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:type) { FactoryGirl::create(:type) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      context "with valid parameters" do
        it "redirects to type" do
          put :update, id: type.to_param, type: { name: 'newname' }
          expect(response).to redirect_to(type_path(type))
        end

        it "assings type" do
          put :update, id: type.to_param, type: { name: 'newname' }
          expect(assigns(:type).name).to eq 'newname'
        end

      end

      context "with invalid parameters" do
        it "shows edit template" do
          put :update, id: type.to_param, type: { name: '' }
          expect(response).to render_template('edit')
        end
      end
    end
  end

  describe "edit" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:type)

      get :edit, id: c.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:type)

      get :edit, id: c.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:type) { FactoryGirl::create(:type) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
      it "assigns type" do
        get :edit, id: type.to_param
        expect(assigns(:type)).to eq type
      end
    end
  end

  describe "destroy" do
    it "redirects non admin" do
      sign_in
      s = FactoryGirl::create(:type)

      delete :destroy, id: s.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      s = FactoryGirl::create(:type)
      delete :destroy, id: s.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:type) { FactoryGirl::create(:type) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      it "destroys type" do
        delete :destroy, id: type.to_param
        expect(Type.count).to eq 0
      end

      it "redirects to artists path" do
        delete :destroy, id: type.to_param
        expect(response).to redirect_to(types_path)
      end
    end
  end
end