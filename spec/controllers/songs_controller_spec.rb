require 'spec_helper'

describe SongsController do
	describe "publish" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:song, status: 'unpublished')

			put :publish, song_id: a.to_param
			expect(response).to redirect_to(root_path) # as user is not allowed to view unpublished item
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:song, status: 'unpublished')

			put :publish, song_id: a.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:song) { FactoryGirl::create(:song, status: 'unpublished') }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
			it "assings song" do
				put :publish, song_id: song.to_param
				expect(assigns(:song).status).to eq 'published'
				expect(response).to redirect_to(song)
			end
		end
	end

	describe "unpublish" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:song, status: 'published')

			put :unpublish, song_id: a.to_param
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:song, status: 'published')

			put :unpublish, song_id: a.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:song) { FactoryGirl::create(:song, status: 'published') }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
			it "assings song" do
				put :unpublish, song_id: song.to_param
				expect(assigns(:song).status).to eq 'unpublished'
				expect(response).to redirect_to(song)
			end
		end
	end

	describe "show" do
		it "assigns song" do
			s = FactoryGirl::build_stubbed(:song)
			Song.stub(:find).and_return s

			get :show, id: s.to_param
			expect(assigns(:song)).to eq s
		end

		it "throws not found exception if non existing song is required" do
			expect { get :show, id: 5555 }.to raise_error(ActiveRecord::RecordNotFound)
		end

		context "unpublished song" do
			let(:author) { FactoryGirl::build_stubbed(:user) }
			let(:song) { FactoryGirl::build_stubbed(:song, status: 'unpublished', user: author) }
			before(:each) { Song.stub(:find).and_return song }

			it "shows to admin" do
				sign_in FactoryGirl::build_stubbed(:user), true
				get :show, id: song

				expect(response.status).to eq 200
			end

			it "does not show to user" do
				sign_in FactoryGirl::build_stubbed(:user)
				get :show, id: song

				expect(response).to redirect_to(root_path)
			end

			it "shows to author" do 
				u = author
				s = FactoryGirl::build_stubbed(:song, status: 'unpublished', user: u)
				s.stub(:user).and_return u
				Song.stub(:find).and_return s
				sign_in u
				get :show, id: s.to_param

				expect(response.status).to eq 200
			end
		end
	end

	describe "new" do
		context "logged in as user" do
			it "assigns new song" do
				sign_in
				get :new
				expect(assigns(:song)).to be_new_record
			end
		end

		context "when not logged in" do
			it "redirects to login page" do
				get :new
				expect(response).to redirect_to new_user_session_path
			end
		end
	end

	describe "create" do
		context "with valid parameters" do
			let(:song) { FactoryGirl::build(:song).attributes }

			context "logged in as user" do
				before(:each) { sign_in }

				it "creates unpublished song" do
					post :create, song: song
					expect(Song.all_unpublished.count).to eq 1
				end

				it "redirects to new unpublished song" do
					post :create, song: song
					expect(response).to redirect_to song_path(Song.all_unpublished.last)
				end
			end

			context "logged in as admin" do
				before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

				it "creates published song" do
					post :create, song: song
					expect(Song.all.count).to eq 1
				end

				it "redirects to new published song" do
					post :create, song: song
					expect(response).to redirect_to song_path(Song.all.last)
				end
			end

			context "when not logged in" do
				it "redirects to login" do
					post :create, song
					expect(response).to redirect_to new_user_session_path
				end
			end
		end

		context "with invalid parameters" do
			let(:song) { FactoryGirl::attributes_for(:song, name: '') }

			context "logged in as user" do
				before(:each) { sign_in }

				it "shows new page with unsaved model" do
					post :create, song: song
					expect(response).to render_template('new')
					expect(assigns(:song)).to be_new_record
				end
			end

			context "logged in as admin" do
				before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

				it "shows new page with unsaved model" do
					post :create, song: song
					expect(response).to render_template('new')
					expect(assigns(:song)).to be_new_record
				end
			end
		end
	end

	describe "update" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:song)

			put :update, id: a.to_param, song: {name: 'newname'}
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:song)

			put :update, id: a.to_param, song: {name: 'newname'}
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:song) { FactoryGirl::create(:song) }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

			context "with valid parameters" do
				it "redirects to song" do
					put :update, id: song.to_param, song: { name: 'newname' }
					expect(response).to redirect_to(song_path(song))
				end

				it "assings song" do
					put :update, id: song.to_param, song: { name: 'newname' }
					expect(assigns(:song).name).to eq 'newname'
				end

			end

			context "with invalid parameters" do
				it "shows edit template" do
					put :update, id: song.to_param, song: { name: '' }
					expect(response).to render_template('edit')
				end
			end
		end
	end

	describe "edit" do
		it "redirects non admin" do
			sign_in
			s = FactoryGirl::create(:song)

			get :edit, id: s.to_param
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			s = FactoryGirl::create(:song)

			get :edit, id: s.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:song) { FactoryGirl::create(:song) }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
			it "assigns song" do
				get :edit, id: song.to_param
				expect(assigns(:song)).to eq song
			end
		end
	end

	describe "destroy" do
		it "redirects non admin" do
			sign_in
			s = FactoryGirl::create(:song)

			delete :destroy, id: s.to_param
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			s = FactoryGirl::create(:song)
			delete :destroy, id: s.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:song) { FactoryGirl::create(:song) }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

			it "destroys song" do
				delete :destroy, id: song.to_param
				expect(Song.count).to eq 0
			end

			it "redirects to artists path" do
				delete :destroy, id: song.to_param
				expect(response).to redirect_to(artists_path)
			end
		end
	end
end
