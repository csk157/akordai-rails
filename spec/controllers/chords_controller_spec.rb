require 'spec_helper'

describe ChordsController do
  describe "show" do
    it "assigns chord" do
      c = FactoryGirl::build_stubbed(:chord)
      Chord.stub(:find).and_return c

      get :show, id: c.to_param
      expect(assigns(:chord)).to eq c
    end

    it "throws not found exception if non existing chord is required" do
      expect { get :show, id: 5555 }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe "new" do
    it "redirects user to homepage" do
      sign_in
      get :new
      expect(response).to redirect_to root_path
    end

    context "when not logged in" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    it "assigns new song for admin" do
      sign_in FactoryGirl::build_stubbed(:user), true
      get :new
      expect(assigns(:chord)).to be_new_record
    end
  end

  describe "create" do
    context "with valid parameters" do
      let(:chord) { FactoryGirl::build(:chord).attributes }

      context "logged in as user" do
        before(:each) { sign_in }

        it "redirects to login page" do
          post :create, chord
          expect(response).to redirect_to root_path
        end
      end

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "creates chord" do
          post :create, chord: chord
          expect(Chord.all.count).to eq 1
        end

        it "redirects to new chord" do
          post :create, chord: chord
          expect(response).to redirect_to chord_path(Chord.all.last)
        end
      end

      context "when not logged in" do
        it "redirects to login page" do
          post :create, chord
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    context "with invalid parameters" do
      let(:chord) { FactoryGirl::attributes_for(:chord, name: '') }

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "shows new page with unsaved model" do
          post :create, chord: chord
          expect(response).to render_template('new')
          expect(assigns(:chord)).to be_new_record
        end
      end
    end
  end

  describe "update" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:chord)

      put :update, id: c.to_param, chord: {name: 'newname'}
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:chord)

      put :update, id: c.to_param, chord: {name: 'newname'}
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:chord) { FactoryGirl::create(:chord) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      context "with valid parameters" do
        it "redirects to chord" do
          put :update, id: chord.to_param, chord: { name: 'newname' }
          expect(response).to redirect_to(chord_path(chord))
        end

        it "assings chord" do
          put :update, id: chord.to_param, chord: { name: 'newname' }
          expect(assigns(:chord).name).to eq 'newname'
        end

      end

      context "with invalid parameters" do
        it "shows edit template" do
          put :update, id: chord.to_param, chord: { name: '' }
          expect(response).to render_template('edit')
        end
      end
    end
  end

  describe "edit" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:chord)

      get :edit, id: c.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:chord)

      get :edit, id: c.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:chord) { FactoryGirl::create(:chord) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
      it "assigns chord" do
        get :edit, id: chord.to_param
        expect(assigns(:chord)).to eq chord
      end
    end
  end

  describe "destroy" do
    it "redirects non admin" do
      sign_in
      s = FactoryGirl::create(:chord)

      delete :destroy, id: s.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      s = FactoryGirl::create(:chord)
      delete :destroy, id: s.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:chord) { FactoryGirl::create(:chord) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      it "destroys chord" do
        delete :destroy, id: chord.to_param
        expect(Chord.count).to eq 0
      end

      it "redirects to artists path" do
        delete :destroy, id: chord.to_param
        expect(response).to redirect_to(chords_path)
      end
    end
  end
end