require 'spec_helper'

describe GenresController do
  describe "new" do
    it "redirects user to homepage" do
      sign_in
      get :new
      expect(response).to redirect_to root_path
    end

    context "when not logged in" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    it "assigns new song for admin" do
      sign_in FactoryGirl::build_stubbed(:user), true
      get :new
      expect(assigns(:genre)).to be_new_record
    end
  end

  describe "create" do
    context "with valid parameters" do
      let(:genre) { FactoryGirl::build(:genre).attributes }

      context "logged in as user" do
        before(:each) { sign_in }

        it "redirects to login page" do
          post :create, genre
          expect(response).to redirect_to root_path
        end
      end

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "creates genre" do
          post :create, genre: genre
          expect(Genre.all.count).to eq 1
        end

        it "redirects to new genre" do
          post :create, genre: genre
          expect(response).to redirect_to genre_path(Genre.all.last)
        end
      end

      context "when not logged in" do
        it "redirects to login page" do
          post :create, genre
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    context "with invalid parameters" do
      let(:genre) { FactoryGirl::attributes_for(:genre, name: '') }

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "shows new page with unsaved model" do
          post :create, genre: genre
          expect(response).to render_template('new')
          expect(assigns(:genre)).to be_new_record
        end
      end
    end
  end

  describe "update" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:genre)

      put :update, id: c.to_param, genre: {name: 'newname'}
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:genre)

      put :update, id: c.to_param, genre: {name: 'newname'}
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:genre) { FactoryGirl::create(:genre) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      context "with valid parameters" do
        it "redirects to genre" do
          put :update, id: genre.to_param, genre: { name: 'newname' }
          expect(response).to redirect_to(genre_path(genre))
        end

        it "assings genre" do
          put :update, id: genre.to_param, genre: { name: 'newname' }
          expect(assigns(:genre).name).to eq 'newname'
        end

      end

      context "with invalid parameters" do
        it "shows edit template" do
          put :update, id: genre.to_param, genre: { name: '' }
          expect(response).to render_template('edit')
        end
      end
    end
  end

  describe "edit" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:genre)

      get :edit, id: c.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:genre)

      get :edit, id: c.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:genre) { FactoryGirl::create(:genre) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
      it "assigns genre" do
        get :edit, id: genre.to_param
        expect(assigns(:genre)).to eq genre
      end
    end
  end

  describe "destroy" do
    it "redirects non admin" do
      sign_in
      s = FactoryGirl::create(:genre)

      delete :destroy, id: s.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      s = FactoryGirl::create(:genre)
      delete :destroy, id: s.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:genre) { FactoryGirl::create(:genre) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      it "destroys genre" do
        delete :destroy, id: genre.to_param
        expect(Genre.count).to eq 0
      end

      it "redirects to artists path" do
        delete :destroy, id: genre.to_param
        expect(response).to redirect_to(genres_path)
      end
    end
  end
end