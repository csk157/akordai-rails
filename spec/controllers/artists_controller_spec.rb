require 'spec_helper'

describe ArtistsController do
	describe "publish" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:artist, status: 'unpublished')

			put :publish, artist_id: a.to_param
			expect(response).to redirect_to(root_path) # as user is not allowed to view unpublished item
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:artist, status: 'unpublished')

			put :publish, artist_id: a.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:artist) { FactoryGirl::create(:artist, status: 'unpublished') }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
			it "assings artist" do
				put :publish, artist_id: artist.to_param
				expect(assigns(:artist).status).to eq 'published'
				expect(response).to redirect_to(artist)
			end
		end
	end

	describe "unpublish" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:artist, status: 'published')

			put :unpublish, artist_id: a.to_param
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:artist, status: 'published')

			put :unpublish, artist_id: a.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:artist) { FactoryGirl::create(:artist, status: 'published') }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
			it "assings artist" do
				put :unpublish, artist_id: artist.to_param
				expect(assigns(:artist).status).to eq 'unpublished'
				expect(response).to redirect_to(artist)
			end
		end
	end

	describe "index" do
		it "assigns published artists" do
			a1 = FactoryGirl::build_stubbed(:artist)
			a2 = FactoryGirl::build_stubbed(:artist)

			Artist.stub(:all_published).and_return [a1, a2]
			get :index

			expect(assigns(:artists)).to eq [a1, a2]
		end
	end

	describe "show" do
		it "assigns artist" do
			a1 = FactoryGirl::build_stubbed(:artist)
			Artist.stub(:find).and_return a1

			get :show, id: 1
			expect(assigns(:artist)).to eq a1
		end

		it "throws not found exception if non existing artist is required" do
			expect { get :show, id: 5555 }.to raise_error(ActiveRecord::RecordNotFound)
		end

		context "unpublished artist" do
			let(:author) { FactoryGirl::build_stubbed(:user) }
			let(:artist) { FactoryGirl::build_stubbed(:artist, status: 'unpublished', user: author) }
			before(:each) { Artist.stub(:find).and_return artist }

			it "shows to admin" do
				sign_in FactoryGirl::build_stubbed(:user), true
				get :show, id: artist

				expect(response.status).to eq 200
			end

			it "does not show to user" do
				sign_in FactoryGirl::build_stubbed(:user)
				get :show, id: artist

				expect(response).to redirect_to(root_path)
			end

			it "shows to author" do 
				u = author
				s = FactoryGirl::build_stubbed(:artist, status: 'unpublished', user: u)
				s.stub(:user).and_return u
				Artist.stub(:find).and_return s
				sign_in u
				get :show, id: s.to_param

				expect(response.status).to eq 200
			end
		end
	end

	describe "new" do
		context "logged in as user" do
			it "assigns new Artist" do
				sign_in
				get :new
				expect(assigns(:artist)).to be_new_record
			end
		end

		context "when not logged in" do
			it "redirects to login page" do
				get :new
				expect(response).to redirect_to new_user_session_path
			end
		end
	end

	describe "create" do
		context "with valid parameters" do
			let(:artist) { FactoryGirl::build(:artist).attributes }

			context "logged in as user" do
				before(:each) { sign_in }

				it "creates unpublished artist" do
					post :create, artist: artist
					expect(Artist.all_unpublished.count).to eq 1
				end

				it "redirects to new unpublished artist" do
					post :create, artist: artist
					expect(response).to redirect_to artist_path(Artist.all_unpublished.last)
				end
			end

			context "logged in as admin" do
				before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

				it "creates published artist" do
					post :create, artist: artist
					expect(Artist.all.count).to eq 1
				end

				it "redirects to new published artist" do
					post :create, artist: artist
					expect(response).to redirect_to artist_path(Artist.all.last)
				end
			end

			context "when not logged in" do
				it "redirects to homepage" do
					post :create, artist
					expect(response).to redirect_to new_user_session_path
				end
			end
		end

		context "with invalid parameters" do
			let(:artist) { FactoryGirl::attributes_for(:artist, name: '') }

			context "logged in as user" do
				before(:each) { sign_in }

				it "shows new page with unsaved model" do
					post :create, artist: artist
					expect(response).to render_template('new')
					expect(assigns(:artist)).to be_new_record
				end
			end

			context "logged in as admin" do
				before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

				it "shows new page with unsaved model" do
					post :create, artist: artist
					expect(response).to render_template('new')
					expect(assigns(:artist)).to be_new_record
				end
			end
		end
	end

	describe "update" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:artist)

			put :update, id: a.to_param, artist: {name: 'newname'}
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:artist)

			put :update, id: a.to_param, artist: {name: 'newname'}
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:artist) { FactoryGirl::create(:artist) }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

			context "with valid parameters" do
				it "redirects to artist" do
					put :update, id: artist.to_param, artist: { name: 'newname' }
					expect(response).to redirect_to(artist_path(artist))
				end

				it "assings artist" do
					put :update, id: artist.to_param, artist: { name: 'newname' }
					expect(assigns(:artist).name).to eq 'newname'
				end

			end

			context "with invalid parameters" do
				it "shows edit template" do
					put :update, id: artist.to_param, artist: { name: '' }
					expect(response).to render_template('edit')
				end
			end
		end
	end

	describe "edit" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:artist)

			get :edit, id: a.to_param
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:artist)

			get :edit, id: a.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:artist) { FactoryGirl::create(:artist) }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
			it "assigns artist" do
				get :edit, id: artist.to_param
				expect(assigns(:artist)).to eq artist
			end
		end
	end

	describe "destroy" do
		it "redirects non admin" do
			sign_in
			a = FactoryGirl::create(:artist)

			delete :destroy, id: a.to_param
			expect(response).to redirect_to(root_path)
		end

		it "redirects not signed in" do
			a = FactoryGirl::create(:artist)
			delete :destroy, id: a.to_param
			expect(response).to redirect_to(new_user_session_path)
		end

		context "as admin" do
			let(:artist) { FactoryGirl::create(:artist) }
			before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

			it "destroys artist" do
				delete :destroy, id: artist.to_param
				expect(Artist.count).to eq 0
			end

			it "redirects to index" do
				delete :destroy, id: artist.to_param
				expect(response).to redirect_to(artists_path)
			end
		end
	end
end
