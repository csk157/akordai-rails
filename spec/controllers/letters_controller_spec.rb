require 'spec_helper'

describe LettersController do

	describe "GET 'index'" do
		it "returns http success" do
			get 'index', letter: 'l'
			response.should be_success
		end

		it "assigns letter model with right letter" do
			get 'index', letter: 'l'
			assigns(:letter).should_not be_nil
			assigns(:letter).letter.should == 'l'
		end

		it "assigns letter model for numbers and signs" do
			get 'index', letter: 'num'
			assigns(:letter).should_not be_nil
			assigns(:letter).letter.should == '#'
		end
	end

end
