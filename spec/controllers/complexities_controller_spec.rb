require 'spec_helper'

describe ComplexitiesController do
  describe "new" do
    it "redirects user to homepage" do
      sign_in
      get :new
      expect(response).to redirect_to root_path
    end

    context "when not logged in" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to new_user_session_path
      end
    end

    it "assigns new song for admin" do
      sign_in FactoryGirl::build_stubbed(:user), true
      get :new
      expect(assigns(:complexity)).to be_new_record
    end
  end

  describe "create" do
    context "with valid parameters" do
      let(:complexity) { FactoryGirl::build(:complexity).attributes }

      context "logged in as user" do
        before(:each) { sign_in }

        it "redirects to login page" do
          post :create, complexity
          expect(response).to redirect_to root_path
        end
      end

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "creates complexity" do
          post :create, complexity: complexity
          expect(Complexity.all.count).to eq 1
        end

        it "redirects to new complexity" do
          post :create, complexity: complexity
          expect(response).to redirect_to complexity_path(Complexity.all.last)
        end
      end

      context "when not logged in" do
        it "redirects to login page" do
          post :create, complexity
          expect(response).to redirect_to new_user_session_path
        end
      end
    end

    context "with invalid parameters" do
      let(:complexity) { FactoryGirl::attributes_for(:complexity, name: '') }

      context "logged in as admin" do
        before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

        it "shows new page with unsaved model" do
          post :create, complexity: complexity
          expect(response).to render_template('new')
          expect(assigns(:complexity)).to be_new_record
        end
      end
    end
  end

  describe "update" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:complexity)

      put :update, id: c.to_param, complexity: {name: 'newname'}
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:complexity)

      put :update, id: c.to_param, complexity: {name: 'newname'}
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:complexity) { FactoryGirl::create(:complexity) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      context "with valid parameters" do
        it "redirects to complexity" do
          put :update, id: complexity.to_param, complexity: { name: 'newname' }
          expect(response).to redirect_to(complexity_path(complexity))
        end

        it "assings complexity" do
          put :update, id: complexity.to_param, complexity: { name: 'newname' }
          expect(assigns(:complexity).name).to eq 'newname'
        end

      end

      context "with invalid parameters" do
        it "shows edit template" do
          put :update, id: complexity.to_param, complexity: { name: '' }
          expect(response).to render_template('edit')
        end
      end
    end
  end

  describe "edit" do
    it "redirects non admin" do
      sign_in
      c = FactoryGirl::create(:complexity)

      get :edit, id: c.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      c = FactoryGirl::create(:complexity)

      get :edit, id: c.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:complexity) { FactoryGirl::create(:complexity) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }
      it "assigns complexity" do
        get :edit, id: complexity.to_param
        expect(assigns(:complexity)).to eq complexity
      end
    end
  end

  describe "destroy" do
    it "redirects non admin" do
      sign_in
      s = FactoryGirl::create(:complexity)

      delete :destroy, id: s.to_param
      expect(response).to redirect_to(root_path)
    end

    it "redirects not signed in" do
      s = FactoryGirl::create(:complexity)
      delete :destroy, id: s.to_param
      expect(response).to redirect_to(new_user_session_path)
    end

    context "as admin" do
      let(:complexity) { FactoryGirl::create(:complexity) }
      before(:each) { sign_in FactoryGirl::build_stubbed(:user), true }

      it "destroys complexity" do
        delete :destroy, id: complexity.to_param
        expect(Complexity.count).to eq 0
      end

      it "redirects to artists path" do
        delete :destroy, id: complexity.to_param
        expect(response).to redirect_to(complexities_path)
      end
    end
  end
end