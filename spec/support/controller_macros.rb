module ControllerMacros
  def sign_in(user = FactoryGirl::build_stubbed(:user), admin = false)
    if user.nil?
      request.env['warden'].stub(:authenticate!).
        and_throw(:warden, {:scope => :user})
      controller.stub :current_user => nil
    else
      user.stub(:admin?) { true } if admin
      request.env['warden'].stub :authenticate! => user
      controller.stub :current_user => user
    end
  end
end

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.include ControllerMacros, :type => :controller
end