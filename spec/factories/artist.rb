FactoryGirl.define do
	sequence :artist_name do |n|
		"jojo#{n}"
	end

	factory :artist do
		name {FactoryGirl.generate :artist_name}
		description 'lalall'
		status 'published'
		language
		user
	end
end
