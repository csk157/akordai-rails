FactoryGirl.define do
  sequence :complexity_name do |n|
    "name#{n}"
  end

  factory :complexity do
    name { FactoryGirl.generate :complexity_name }
  end
end
