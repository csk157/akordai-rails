FactoryGirl.define do
	sequence :song_name do |n|
		"Song num #{n}"
	end

	factory :song do
		name {FactoryGirl.generate :song_name}
		text 'dadad'
		status 'published'
		language
		user
		genre
		type
		complexity
	end
end
