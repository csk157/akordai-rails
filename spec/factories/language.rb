FactoryGirl.define do
	sequence :language_name do |n|
		"name#{n}"
	end
	sequence :language_code do |n|
		"nm#{n}"
	end

	factory :language do
		sequence(:name){|n| "lala#{n}" }
		sequence(:code){|n| "ll#{n}" }
	end
end