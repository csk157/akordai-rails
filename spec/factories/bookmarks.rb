FactoryGirl.define do
  factory :bookmark do
    user_id 1
    bookmarkable_type "Artist"
    bookmarkable_id 1
    note "MyText"
  end
end
