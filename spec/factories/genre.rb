FactoryGirl.define do
	sequence :genre_name do |n|
		"name#{n}"
	end
	factory :genre do
		name {FactoryGirl.generate :genre_name}
	end
end