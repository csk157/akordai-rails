# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :vote do
    score
    user
    ip "192.168.0.1"
    up false
  end
end
