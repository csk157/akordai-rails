FactoryGirl.define do
	sequence :email do |n|
		"emaiadal#{n}@factory.com"
	end

	factory :user do
    email {FactoryGirl.generate :email}
		name "alalala lalal"
		password "foobarasdasd"
		password_confirmation "foobarasdasd"
	end
end