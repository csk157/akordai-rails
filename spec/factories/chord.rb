FactoryGirl.define do
	sequence :chord_name do |n|
		"chord#{n}"
	end
	factory :chord do
		name {FactoryGirl.generate :chord_name}
    notation 'x02211'
	end
end