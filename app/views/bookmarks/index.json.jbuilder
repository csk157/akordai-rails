json.array!(@bookmarks) do |bookmark|
  json.extract! bookmark, :id, :user_id, :bookmarkable_type, :bookmarkable_id, :note
  json.url bookmark_url(bookmark, format: :json)
end
