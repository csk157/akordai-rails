class ArtistsController < ApplicationController
	add_breadcrumb I18n.t('breadcrumbs.artists'), :artists_path
	before_action :set_artist, only: [:show, :edit, :update, :destroy]
	before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy, :publish, :unpublish, :unpublished]
	before_filter :admin_protect!, only: [:edit, :update, :destroy, :publish, :unpublish, :unpublished]

	def search
		query = params[:q]
		return if query.length < 2

		respond_to do |format|
			artists = Search.search_artists(query)
			artists = artists.map do |s|
				s.serializable_hash(
					only: [:name, :id, :slug],
					methods: [:thumb_url, :url]
				) 
			end
			
			format.json { render json: artists }
		end
	end

	# GET /artists
	# GET /artists.json
	def index
		if params[:q].nil?
			if params[:letter].present?
				@artists = Artist.find_by_letter params[:letter]
			else
				@artists = Artist.all_published
			end
		end

		respond_to do |format|
			format.html if params[:q].nil?
			format.json do
				if !params[:q].nil? && params[:q].length >= 2
					render json: Artist.suggestions(params[:q]).map(&:attributes)
				else
					render json: []
				end
			end
		end
	end

	def unpublished
		add_breadcrumb I18n.t('artists.unpublished'), :unpublished_artists_path
		@artists = Artist.all_unpublished

		respond_to do |format|
			format.html
			format.json { render json: {artists: @artists } }
		end
	end

	def publish
		@artist = Artist.find(params[:artist_id])
		res = @artist.publish

		respond_to do |format|
			if res
				format.html { redirect_to @artist, notice: t('artists.notices.published') }
				format.json { head :no_content }
			else
				format.html { redirect_to @artist, alert: t('artists.notices.published_error') }
				format.json { render json: @artist.errors, status: :unprocessable_entity }
			end
		end
	end

	def unpublish
		@artist = Artist.find(params[:artist_id])
		res = @artist.unpublish

		respond_to do |format|
			if res
				format.html { redirect_to @artist, notice: t('artists.notices.unpublished') }
				format.json { head :no_content }
			else
				format.html { redirect_to @artist, alert: t('artists.notices.unpublished_error') }
				format.json { render json: @artist.errors, status: :unprocessable_entity }
			end
		end
	end

	# GET /artists/1
	# GET /artists/1.json
	def show
		@artist = Artist.find(params[:id], include: [{songs: [:type, :score, :artists, :genre]}] )
		return redirect_to @artist, :status => :moved_permanently if request.path != artist_path(@artist)
		return redirect_to root_path if !@artist.published? && !(admin? || @artist.user == current_user)
	end

	# GET /artists/new
	def new
		@artist = Artist.new
		add_breadcrumb I18n.t('breadcrumbs.create'), :new_artist_path
	end

	# GET /artists/1/edit
	def edit
		add_breadcrumb I18n.t('breadcrumbs.edit'), edit_artist_path(@artist)
	end

	# POST /artists
	# POST /artists.json
	def create
		@artist = Artist.new(artist_params)
		@artist.assign_user(current_user)

		respond_to do |format|
			if @artist.save
				format.html { redirect_to @artist, notice: 'Artist was successfully created.' }
				format.json { render 'show', status: :created, location: @artist }
			else
				format.html { render 'new' }
				format.json { render json: @artist.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /artists/1
	# PATCH/PUT /artists/1.json
	def update
		respond_to do |format|
			if @artist.update(artist_params)
				format.html { redirect_to @artist, notice: 'Artist was successfully updated.' }
				format.json { head :no_content }
			else
				format.html { render 'edit' }
				format.json { render json: @artist.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /artists/1
	# DELETE /artists/1.json
	def destroy
		@artist.destroy
		respond_to do |format|
			format.html { redirect_to artists_url }
			format.json { head :no_content }
		end
	end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_artist
			@artist = Artist.find(params[:id])
			add_breadcrumb @artist.name, artist_path(@artist)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def artist_params
			params.require(:artist).permit(:name, :description, :language_id, :image)
    end
end
