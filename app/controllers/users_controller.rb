class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  # GET /users
  # GET /users.json
  def index
    add_breadcrumb I18n.t('breadcrumbs.users'), :users_path
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    add_breadcrumb I18n.t('breadcrumbs.users'), :users_path
    add_breadcrumb @user.name, user_path(@user)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end
end
