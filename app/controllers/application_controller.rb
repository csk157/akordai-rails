class ApplicationController < ActionController::Base
  helper_method :forem_user

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :set_breadcrumb
  helper_method :admin?

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :email, :password, :password_confirmation, :avatar)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
       u.permit(:name, :email, :password, :password_confirmation, :avatar)
    end
  end

  def admin_protect!
    # raise ActionController::RoutingError.new(t 'user.not_authorized') unless current_user.admin?
    redirect_to root_path, alert: t('user.not_authorized') unless current_user.admin?
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  def admin?
    return current_user && current_user.admin?
  end

  def forem_user
    current_user
  end

  def set_breadcrumb
    add_breadcrumb I18n.t('breadcrumbs.home'), :root_path
  end
end
