class BookmarksController < ApplicationController
  before_filter :authenticate_user!, only: [:index, :create, :destroy]

  # GET /bookmarks
  # GET /bookmarks.json
  def index
    # add_breadcrumb I18n.t('breadcrumb.bookmarks'), :bookmarks
    type = params[:type] || 'artists'

    @bookmarks = []

    case type
    when 'songs'
      @bookmarks = Bookmark.songs(current_user.id)
      add_breadcrumb I18n.t('breadcrumbs.bookmarked_songs'), bookmarks_path('songs')
    when 'chords'
      @bookmarks = Bookmark.chords(current_user.id)
      add_breadcrumb I18n.t('breadcrumbs.bookmarked_chords'), bookmarks_path('chords')
    when 'users'
      @bookmarks = Bookmark.users(current_user.id)
      add_breadcrumb I18n.t('breadcrumbs.bookmarked_users'), bookmarks_path('users')
    else
      @bookmarks = Bookmark.artists(current_user.id)
      add_breadcrumb I18n.t('breadcrumbs.bookmarked_artists'), bookmarks_path('artists')
    end
  end

  # POST /bookmarks
  # POST /bookmarks.json
  def create
    @bookmark = Bookmark.new(bookmark_params)
    @bookmark.user_id = current_user.id

    respond_to do |format|
      if @bookmark.save
        format.html { redirect_to @bookmark, notice: 'Bookmark was successfully created.' }
        format.json { render action: 'show', status: :created, location: @bookmark }
      else
        format.html { render action: 'new' }
        format.json { render json: @bookmark.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookmarks/1
  # DELETE /bookmarks/1.json
  def destroy
    Bookmark.where(user_id: current_user.id, bookmarkable_type: params[:bookmarkable_type], bookmarkable_id: params[:bookmarkable_id]).destroy_all
    respond_to do |format|
      format.html { redirect_to bookmarks_url }
      format.json { head :no_content }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def bookmark_params
      params.require(:bookmark).permit(:bookmarkable_type, :bookmarkable_id, :note)
    end
end
