class ChordsController < ApplicationController
  add_breadcrumb I18n.t('breadcrumbs.chords'), :chords_path
  before_action :set_chord, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_filter :admin_protect!, only: [:new, :create, :edit, :update, :destroy]

  def search
    query = params[:q]
    return if query.length < 1

    respond_to do |format|
      chords = Search.search_chords(query)
      chords = chords.map do |s|
        s.serializable_hash(
          only: [:name, :id],
          methods: [:url]
        ) 
      end
      
      format.json { render json: chords }
    end
  end

  # GET /chords
  # GET /chords.json
  def index
    @chords = Chord.all
  end

  # GET /chords/1
  # GET /chords/1.json
  def show
    # @chord = Chord.find(params[:id], include: [{songs: [:type, :score, :artists, :genre]}])
    @songs = @chord.published_songs.includes([:type, :score, :artists, :genre]).paginate(page: params[:page], per_page: 30 )
    # .paginate(:page => params[:page], :per_page => 30)
    add_breadcrumb @chord.name, chord_path(@chord)
  end

  # GET /chords/new
  def new
    add_breadcrumb I18n.t('breadcrumbs.create'), new_chord_path
    @chord = Chord.new
  end

  # GET /chords/1/edit
  def edit
    add_breadcrumb @chord.name, chord_path(@chord)
    add_breadcrumb I18n.t('breadcrumbs.edit'), edit_chord_path(@chord)
  end

  # POST /chords
  # POST /chords.json
  def create
    @chord = Chord.new(chord_params)

    respond_to do |format|
      if @chord.save
        format.html { redirect_to @chord, notice: 'Chord was successfully created.' }
        format.json { render 'show', status: :created, location: @chord }
      else
        format.html { render 'new' }
        format.json { render json: @chord.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chords/1
  # PATCH/PUT /chords/1.json
  def update
    respond_to do |format|
      if @chord.update(chord_params)
        format.html { redirect_to @chord, notice: 'Chord was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render 'edit' }
        format.json { render json: @chord.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chords/1
  # DELETE /chords/1.json
  def destroy
    @chord.destroy
    respond_to do |format|
      format.html { redirect_to chords_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chord
      @chord = Chord.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chord_params
      params.require(:chord).permit(:parent_id, :name, :notation)
    end
end
