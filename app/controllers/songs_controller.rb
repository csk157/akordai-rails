class SongsController < ApplicationController
	before_action :set_song, only: [:show, :edit, :update, :destroy]
	before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy, :unpublished, :publish, :unpublish]
	before_filter :admin_protect!, only: [:edit, :update, :destroy, :unpublished, :publish, :unpublish]

	def search
		query = params[:q]
		return if query.length < 2

		respond_to do |format|
			songs = Search.search_songs(query)
			songs = songs.map do |s|
				s.serializable_hash(
					include: { 
						artists: { 
							only: [:name],
							methods: [:thumb_url]
						} 
					},
					only: [:name, :id, :slug],
					methods: [:url]
				) 
			end

			format.json { render json: songs }
		end
	end

	# GET /songs/1
	# GET /songs/1.json
	def show
		return redirect_to @song, :status => :moved_permanently if request.path != song_path(@song)
		return redirect_to root_path if !@song.published? && !(admin? || @song.user == current_user)

		add_breadcrumb @song.name, @song
		respond_to do |format|
			format.html
			format.json { render json: {song: @song } }
		end
	end

	def unpublished
		add_breadcrumb I18n.t('songs.unpublished'), :unpublished_songs_path
		@songs = Song.all_unpublished

		respond_to do |format|
			format.html
			format.json { render json: {songs: @songs } }
		end
	end

	def publish
		@song = Song.find(params[:song_id])
		res = @song.publish

		respond_to do |format|
			if res
				format.html { redirect_to @song, notice: t('songs.notices.published') }
				format.json { head :no_content }
			else
				format.html { redirect_to @song, alert: t('songs.notices.published_error') }
				format.json { render json: @song.errors, status: :unprocessable_entity }
			end
		end
	end

	def unpublish
		@song = Song.find(params[:song_id])
		res = @song.unpublish

		respond_to do |format|
			if res
				format.html { redirect_to @song, notice: t('songs.notices.unpublished') }
				format.json { head :no_content }
			else
				format.html { redirect_to @song, alert: t('songs.notices.unpublished_error') }
				format.json { render json: @song.errors, status: :unprocessable_entity }
			end
		end
	end

	# GET /songs/new
	def new
		add_breadcrumb I18n.t('breadcrumbs.new_song'), :new_song_path
		@song = Song.new
	end

	# GET /songs/1/edit
	def edit
		add_breadcrumb @song.name, song_path(@song)
		add_breadcrumb I18n.t('breadcrumbs.edit'), edit_song_path(@song)
	end

	# POST /songs
	# POST /songs.json
	def create
		@song = Song.new(song_params)
		@song.assign_user(current_user)
		respond_to do |format|
			if @song.save
				format.html { redirect_to @song, notice: 'Song was successfully created.' }
				format.json { render 'show', status: :created, location: @song }
			else
				format.html { render 'new' }
				format.json { render json: @song.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /songs/1
	# PATCH/PUT /songs/1.json
	def update
		respond_to do |format|
			if @song.update(song_params)
				format.html { redirect_to @song, notice: 'Song was successfully updated.' }
				format.json { head :no_content }
			else
				format.html { render 'edit' }
				format.json { render json: @song.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /songs/1
	# DELETE /songs/1.json
	def destroy
		@song.destroy
		respond_to do |format|
			format.html { redirect_to artists_path }
			format.json { head :no_content }
		end
	end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_song
		@song = Song.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def song_params
		params.require(:song).permit(:user_id, :genre_id, :type_id,
			:language_id, :name, :text, :artist_tokens, :complexity_id)
    end
end
