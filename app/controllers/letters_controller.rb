class LettersController < ApplicationController
  def index
	  @letter = Letter.new(params[:letter])
	  @selected_letter = @letter.letter

    add_breadcrumb I18n.t('breadcrumbs.letter', letter: @letter.letter.capitalize), letter_path(params[:letter])
  end
end
