class SearchesController < ApplicationController
  def search
    @search = Search.new params[:q]
    add_breadcrumb "Search for #{@search.query}", nil
  end
end
