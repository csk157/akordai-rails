class ScoresController < ApplicationController
  def vote
    score = Score.find(params[:id])
    results = score.vote_with_results(current_user, request.remote_ip, params[:up])

    respond_to do |format|
      format.json { render json: results }
    end
  end

  def voted
    score = Score.find(params[:id])
    vote = score.find_vote(current_user, request.remote_ip)

    respond_to do |format|
      format.json { render json: { vote: vote } }
    end
  end
end
