// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
//= require jquery.tokeninput
//= require jquery.autosize
//= require turbolinks
//= require bootstrap
//= require raphael-min
//= require jtab
//= require jquery.qtip.min
//= require jquery.truncator
//= require hogan-2.0.0
//= require typeahead
//= require forem
//= require_self
//= require_tree .

$(document).on('page:load', init);
$(document).ready(init);

function init() {
  initializeVoting();
  initializeGlobalSearch();
  initializeBookmarks();
}

function initializeBookmarks() {
  $('.js-create-bookmark').on('click', function(e) {
    obj = $(this);
    var type = obj.data('type'),
      id = obj.data('id');

    $.post(HOST + 'bookmarks.json', { bookmark: {bookmarkable_type: type, bookmarkable_id: id} }, function(data) {
      obj.addClass('hidden');
      $('.js-delete-bookmark').removeClass('hidden');
    }, 'json');
  });

  $('.js-delete-bookmark').on('click', function(e) {
    obj = $(this);
    var type = obj.data('type'),
      id = obj.data('id');

    $.ajax(HOST + 'bookmarks.json',
      {
        type: 'DELETE',
        data: { bookmarkable_type: type, bookmarkable_id: id},
        success: function(data) {
          obj.addClass('hidden');
          $('.js-create-bookmark').removeClass('hidden');
        },
        dataType: 'json'
      }
    );
  });
}

function initializeVoting(){
  $('.js-vote').on('click', function(e) {
    var scoreId = $(this).data('score'), up = $(this).data('up');
    $.post(HOST + 'scores/' + scoreId + '/vote.json', { up: up }, function(data) {
      if (data.success) {
        $('.js-vote-result').text(data.score);
        
        if (data.score < 0) {
          $('.js-vote-result').removeClass('positive');
          $('.js-vote-result').addClass('negative');
        } else {
          $('.js-vote-result').addClass('positive');
          $('.js-vote-result').removeClass('negative');
        }

        if(up == true) {
          $('.vote-down').addClass('deactivated');
          $('.vote-up').removeClass('deactivated');
        } else {
          $('.vote-up').addClass('deactivated');
          $('.vote-down').removeClass('deactivated');
        }
      }
    });
  });
}

function initializeGlobalSearch() {
  var songTpl = '';
  songTpl += '<div class="search-result-song">';
  songTpl += '  <span class="song-title">{{ name }}</span>';
  songTpl += '  <div class="song-artists">';
  songTpl += '    {{#artists}}';
  songTpl += '      <div class="song-artist">';
  songTpl += '        <img src="assets/{{thumb_url}}" width="25" height="25" />';
  songTpl += '        {{name}}';
  songTpl += '      </div>';
  songTpl += '    {{/artists}}';
  songTpl += '  </div>';
  songTpl += '</div>';

  var artistTpl = '';
  artistTpl += '<div class="search-result-artist">';
  artistTpl += '  <img src="assets/{{thumb_url}}" width="25" height="25" />';
  artistTpl += '  {{name}}';
  artistTpl += '</div>';

  $('.js-global-search').typeahead([
    {
      name: "songs",
      minLength: 2,
      limit: 5,
      remote: {
        url: HOST + 'songs/search/%QUERY.json',
        filter: function(data) {
          retval = [];
          for (var i = 0;  i < data.length;  i++) {
              retval.push({
                  value: data[i].name,
                  tokens: [data[i].name],
                  name: data[i].name,
                  artists: data[i].artists,
                  url: data[i].url
              });
          }
          return retval;
        }
      },
      template: songTpl,
      header: '<h3 class="search-group-title">Songs</h3>',
      engine: Hogan
    },
    {
      name: "artists",
      minLength: 2,
      limit: 5,
      remote: {
        url: HOST + 'artists/search/%QUERY.json',
        filter: function(data) {
          retval = [];
          for (var i = 0;  i < data.length;  i++) {
              retval.push({
                  value: data[i].name,
                  tokens: [data[i].name],
                  name: data[i].name,
                  thumb_url: data[i].thumb_url,
                  url: data[i].url
              });
          }
          return retval;
        }
      },
      header: '<h3 class="search-group-title">Artists</h3>',
      template: artistTpl,
      engine: Hogan
    },
    {
      name: "chords",
      minLength: 1,
      limit: 5,
      remote: {
        url: HOST + 'chords/search/%QUERY.json',
        filter: function(data) {
          retval = [];
          for (var i = 0;  i < data.length;  i++) {
              retval.push({
                  value: data[i].name,
                  tokens: [data[i].name],
                  name: data[i].name,
                  url: data[i].url
              });
          }
          return retval;
        }
      },
      header: '<h3 class="search-group-title">Chords</h3>'
    },
  ]).on('typeahead:selected', function($e, datum) {
    window.location = datum.url;
  });


}