$(document).ready(initializeSongs);
$(document).on('page:load', initializeSongs);

function markChords(text) {
  var search = /\[(([A-G])([5679bm#]|1[13]|6\/9|7[\-#+b][59]|7?sus[24]|add[249]|aug|dim7?|m\/maj7|m1[13]|m[679]|m7?b5|maj1[13]|maj[79])*)\]/gm;
  var result = text.replace(search, '<span class="chord show-tooltip" data-tooltip="$1">$1</span>');
  
  return result;
}

function markTabs(text) {
  var search = /\[(\$[0-9\.\/\$a-zA-Z\| ]*)\]/gm;
  var result = text.replace(search, '<div class="tab">$1</div>');
  
  return result;
}

function markSong(element){
  if($(element).length > 0){
    var marked = markChords($(element).text());
    marked = markTabs(marked);

    $(element).html(marked);

    $(element + ' .tab').each(function(){
      jtab.render($(this), $(this).text());
    });

    $(element + ' .chord.show-tooltip').qtip({ // Grab some elements to apply the tooltip to
        content: {
          text: ' ',
          title: '<span class="glyphicon glyphicon-move"></span> Click to drag',
          button: 'close'
        },
        style: {classes: 'qtip-bootstrap'},
        show: 'click mouseenter',
        hide: 'unfocus mouseleave', // Unfocus = click anywhere but tooltip & target
        events: {
          show: function(event, tip) {
            tip.set('content.text', '<div class="jtab chordonly render-chord"></div>');
            tip.elements.target.click(function(e) {
              tip.wasClicked = (e.type === 'click');
              if(tip.wasClicked)
                tip.elements.tooltip.draggable();
            });

            tip.elements.button.click(function(e){
              // force close
              tip.forceClose = true;
              tip.hide();
            });
          },
          visible: function(event, tip) {
            jtab.render($('#'+tip.tooltip.attr('id')+' .render-chord'), tip.target.data('tooltip'));
          },
          hide: function(event, tip) {
             // If we're hiding and it was previously clicked... stop!
             if(tip.forceClose){
              tip.wasClicked = false;
              tip.forceClose = false;
              return true;
             }
             if(tip.wasClicked) try{ event.preventDefault(); } catch(e) {}
             
             // Reset the clicked flag
          }
        }
    });
  }
}

function enableSimilarSearch() {
  var obj = $("#js-song-name");

  if(obj.length === 0) return;

  obj.on('keyup', function(e) {
    if(obj.val().length > 2){
      $.get(HOST + 'songs/search.json', {q: obj.val()}, function(data) {
        showVersioningSuggestions(data);
      });
    }
  });
}

function showVersioningSuggestions(data) {
  var obj = $("#js-song-version-suggestions");

  if(obj.length === 0)
    return;

  obj.html('');
  for(var i = 0; i < data.length; i++) {
    var song = data[i], content = '';
    content += '<li class="list-group-item">';
    content += '  <div class="row">';
    content += '    <div class="col-md-2">';
    content += '      <input type="checkbox" name="version" value="'+song.id+'" class="js-select-version" />';
    content += '    </div>';
    content += '    <div class="col-md-10">';
    content += '      <a href="/songs/'+song.slug+'" target="_blank"><span id="song-version-'+song.id+'">'+song.name+'</span> <span class="icon-external-link"></span></a>';
    content += '    </div>';
    content += '  </div>';
    content += '</li>';
    obj.append(content);
  }

  $('[name=version]').change(function(){
    if(this.checked) {
      $('.js-select-version.checked').prop('checked', false);
      $('.js-select-version').removeClass('checked');
      $(this).addClass('checked');

      var name = $('#song-version-'+$(this).val()).text();
      $('#js-song-name').val(name).prop('disabled', true);
    } else {
      $(this).removeClass('checked');
      $('#js-song-name').prop('disabled', false);
    }
  });
}

function initializeSongs(){
  jtab.renderimplicit();
  $('#js-artist-tokens').tokenInput('/artists.json',
    { crossDomain: false, theme: 'facebook', prePopulate: $('#js-artist-tokens').data('pre'), preventDuplicates: true });

  $('.js-chords-song').autosize();
  $('.js-chords-song').on('keyup', function(){
    $('.js-chords-preview').text($(this).val());
    markSong('.js-chords-preview');
  });

  $('.js-chords-song').keyup();

  markSong('.preview-song');
  enableSimilarSearch();
}