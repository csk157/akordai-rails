module ApplicationHelper
	def title(page_title)
	  content_for(:title) { page_title }
	end

	def description(desc)
		content_for(:description) { desc }
	end

	def keywords(keyw)
		content_for(:keywords) { keyw }
	end
end
