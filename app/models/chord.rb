class Chord < ActiveRecord::Base
	extend FriendlyId
	default_scope { order 'chords.name ASC' }
	has_and_belongs_to_many :songs
	belongs_to :parent, :class_name => 'Chord', :foreign_key => 'parent_id'
	has_many :children, :class_name => 'Chord', :foreign_key => 'parent_id', :dependent => :destroy

	friendly_id :name, use: [:slugged, :finders, :history] # you can now do MyClass.find('bar')

	validates :name, presence: true, uniqueness: true
	validates :notation, presence: true, format: {with: /\A[0-9x]{6}\Z/}
	validates :parent, presence: true, unless: 'parent_id.blank?'
	after_save :relate_songs

	def url
		Rails.application.routes.url_helpers.chord_path(self)
	end

	def published_songs
		self.songs.where status: 'published'
	end

	private
	def relate_songs
		self.songs.delete_all
		Song.all.each do |s|
			self.songs << s if s.has_chord?(self.name)
		end
	end
end
