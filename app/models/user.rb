class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :token_authenticatable, :confirmable,
	# :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable, :confirmable,
	  :recoverable, :rememberable, :trackable, :validatable
	devise :omniauthable, :omniauth_providers => [:facebook]

	validates :name, presence: true
	has_many :songs
	has_many :artists
	has_attached_file :avatar, :styles => { medium: "300>", small: "50x50#", tiny: "25x25#" }, default_url: "default_:style.gif"

	def admin?
		return is_admin
	end

	def self.new_with_session(params, session)
		super.tap do |user|
			if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
				user.email = data["email"] if user.email?
			end
		end
	end

	def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
		user = User.where(:provider => auth.provider, :uid => auth.uid).first
		unless user
			user = User.create(name: auth.extra.raw_info.name,
				provider: auth.provider,
				uid: auth.uid,
				email: auth.info.email,
				password: Devise.friendly_token[0,20]
			)
		end
		user
	end

	def to_s
		name
	end

	def bookmarked?(bookmarkable)
		Bookmark.bookmarked?(self, bookmarkable)
	end
end
