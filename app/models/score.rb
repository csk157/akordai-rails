class Score < ActiveRecord::Base
  belongs_to :scorable, polymorphic: true
  has_many :votes, dependent: :destroy

  validates :scorable, presence: true, associated: true

  def positive?
    return score >= 0
  end

  def score
    return up - down
  end

  def vote_with_results(user, ip, up)
    vote = self.find_vote(user, ip)

    result = true
    if vote
      result = vote.change_to(up == 'true' ? true : false)
    else
      vote = self.votes.build(ip: ip, user: user, up: up)
      result = vote.save
    end
    self.reload

    return { success: result, score: self.score }
  end

  def find_vote(user, ip)
    v = nil
    if user
      v = self.votes.where('ip = ? OR user_id = ?', ip, user.id)
    else
      v = self.votes.where('ip = ?', ip)
    end

    return v ? v.first : nil
  end

  def result
    res = "#{score}"

    if positive?
      res = "+#{res}"
    end

    return res
  end
end
