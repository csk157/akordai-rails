class Letter
	attr_accessor :letter, :artists, :songs, :filled

	def initialize(l)
		@artists = []
		@songs = []
		@filled = false

		if(l == 'num')
			@letter = '#'
		else
			@letter = l
		end

		fill
	end

	def fill
		unless @filled
			@artists = Artist.find_by_letter(letter)
			@songs = Song.find_by_letter(letter)
			@filled = true
		end
	end

	def get_all
		fill
		return {artists: artists, songs: songs}
	end

end
