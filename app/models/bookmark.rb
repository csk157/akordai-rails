class Bookmark < ActiveRecord::Base
	belongs_to :user
	belongs_to :bookmarkable, polymorphic: true

	validates :user, presence: true
	validates :bookmarkable, presence: true
	validates :bookmarkable_id, uniqueness: { scope: [:user_id, :bookmarkable_id, :bookmarkable_type] }

	def self.artists(user_id)
		return self.where(user_id: user_id, bookmarkable_type: 'Artist').includes(:bookmarkable)
	end
	
	def self.songs(user_id)
		return self.where(user_id: user_id, bookmarkable_type: 'Song').includes(:bookmarkable)
	end
	
	def self.chords(user_id)
		return self.where(user_id: user_id, bookmarkable_type: 'Chord').includes(:bookmarkable)
	end
	
	def self.users(user_id)
		return self.where(user_id: user_id, bookmarkable_type: 'User').includes(:bookmarkable)
	end

	def self.bookmarked?(user, bookmarkable) 
		return self.where(user_id: user.id, bookmarkable_type: bookmarkable.class.name, bookmarkable_id: bookmarkable.id).count == 1
	end
end
