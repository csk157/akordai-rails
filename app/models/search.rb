class Search
  attr_accessor :query, :songs, :artists

  def self.search_artists(q)
    Artist.all_published.where('lower(artists.name) LIKE ?', "%#{q.downcase}%")
  end

  def self.search_songs(q)
    Song.all_published.where('lower(songs.name) LIKE ?', "%#{q.downcase}%")
  end

  def self.search_chords(q)
    Chord.where('lower(chords.name) LIKE ?', "%#{q.downcase}%")
  end

  def initialize(q)
    @query = q
    @songs = @artists = []
    search
  end

  private
  def search
    # begin
    #   require 'sunspot_solr'
    #   q = @query # WTF? Otherwise does not work
    #   song_search =  Song.search do
    #     fulltext q do
    #       boost_fields name: 2.0
    #     end
    #   end
    #   @songs = song_search.results

    #   artist_search =  Artist.search do
    #     fulltext q
    #   end
    #   @artists = artist_search.results

    # rescue LoadError
      @songs = Song.all_published.where('lower(name) LIKE ?', "%#{@query.downcase}%")
      @artists = Artist.all_published.where('lower(name) LIKE ?', "%#{@query.downcase}%")
    # end
  end
end
