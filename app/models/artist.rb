class Artist < ActiveRecord::Base
	extend FriendlyId
	default_scope { order 'artists.name ASC' }

	has_and_belongs_to_many :songs
	belongs_to :user
	belongs_to :language
	has_one :score, as: :scorable, dependent: :destroy
	has_attached_file :image, styles: { medium: "300>", small: "50x50#", tiny: "25x25#" }, default_url: "default_:style.gif"

	validates :name, presence: true
	validates :language, presence: true
	validates :user, presence: true
	validates :youtube, url: true, allow_blank: true
	validates :twitter, url: true, allow_blank: true
	validates :facebook, url: true, allow_blank: true
	validates :wikipedia, url: true, allow_blank: true
	validates :vkontakte, url: true, allow_blank: true
	validates_inclusion_of :status, :in => %w[published unpublished]

	after_create :create_score_association

	friendly_id :name, use: [:slugged, :finders, :history] # you can now do MyClass.find('bar')

	# searchable do
	# 	text :name
	# end

	def url
		Rails.application.routes.url_helpers.artist_path(self)
	end

	def thumb_url
		image.url(:tiny)
	end

	def self.all_unpublished
		self.where status: 'unpublished'
	end

	def self.all_published
		self.where status: 'published'
	end

	def published_songs
		self.songs.where(status: 'published')
	end

	def self.find_by_letter(letter)
		artists = []
		if(letter.blank? || letter.length > 1)
			artists = self.all_published
		elsif letter == '#' # Return starting with numbers or non-standard characters
			artists = self.all_published.where 'name ~ ?', "^[^A-Za-z_].*"
		else
			artists = self.all_published.where 'lower(name) LIKE ?', "#{letter.downcase}%"
		end

		return artists
	end

	def has_links?
		return self.wikipedia || self.youtube || self.facebook || self.twitter || self.vkontakte
	end

	def assign_user(user)
		self.user = user

		self.status = 'unpublished'
		self.status = 'published' if user.admin?
	end

	def self.suggestions(query)
		self.all_published.where('lower(name) LIKE ?', "%#{query.downcase}%")
	end

	def published?
		self.status == 'published'
	end

	def publish
		self.status = 'published'
		self.save
	end

	def unpublish
		self.status = 'unpublished'
		self.save
	end

	private
	def create_score_association
		self.create_score
	end
end
