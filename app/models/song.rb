class Song < ActiveRecord::Base
	extend FriendlyId
	attr_reader :artist_tokens
	default_scope { order 'songs.name ASC' }
	has_and_belongs_to_many :artists
	has_and_belongs_to_many :chords
	belongs_to :user
	belongs_to :type
	belongs_to :genre
	belongs_to :language
	belongs_to :complexity
	has_one :score, as: :scorable, dependent: :destroy
	# acts_as_taggable

	validates :name, :text, :user, :language, :type, :genre, presence: true
	validates :complexity, presence: true
	validates_inclusion_of :status, :in => %w[published unpublished]
	before_create :assign_version
	after_create :create_score_association
	before_destroy :fix_versions
	after_save :relate_chords

	friendly_id :generate_slug, use: [:slugged, :finders, :history] # you can now do MyClass.find('bar')

	def url
		Rails.application.routes.url_helpers.song_path(self)
	end

	def self.all_published
		where status: 'published'
	end

	def self.all_unpublished
		where status: 'unpublished'
	end

	def artist_tokens=(ids)
		self.artist_ids = ids.split(",")
	end

	def should_generate_new_friendly_id?
		return new_record? || slug == generate_slug
	end

	def assign_user(user)
		self.user = user

		self.status = 'unpublished'
		self.status = 'published' if user.admin?
	end

	def generate_slug
		result = ''
		art = artists

		result = "#{artists.map(&:name).join(',')} - " if art.size > 0
		result = result + "#{name}"

		result = result + " v#{version+1}" if version > 0

		return result
	end

	def artists_names
		return "#{artists.map(&:name).join(', ')}"
	end

	def self.find_by_letter(letter)
		songs = []
		if(letter.blank? || letter.length > 1)
			songs = self.all_published.includes([:type, :score, :artists, :genre])
		elsif letter == '#' # Return starting with numbers or non-standard characters
			songs = self.all_published.references(:song).where('songs.name ~ ?', "^[^A-Za-z_].*").includes([:type, :score, :artists, :genre])
		else
			songs = self.all_published.references(:song).where('lower(songs.name) LIKE ?', "#{letter}%").includes([:type, :score, :artists, :genre])
		end
		return songs
	end

	def has_chord?(chord)
		self.text.include?("[#{chord}]")
	end

	def published?
		self.status == 'published'
	end

	def publish
		self.status = 'published'
		self.save
	end

	def unpublish
		self.status = 'unpublished'
		self.save
	end

	private
	def create_score_association
		self.create_score
	end

	def relate_chords
		self.chords.delete_all
		Chord.all.each do |c|
			self.chords << c if self.has_chord?(c.name)
		end
	end

	def assign_version
		self.version = 0

		similar = []
		if self.artist_ids.empty?
			similar = Song.all_published.where(name: self.name).order('version DESC')
		else
			similar = Song.all_published.where(name: self.name).order('version DESC').joins(:artists).where("artists.id IN (#{self.artist_ids.join(',')})")
		end

		if similar.size > 0
			similar = similar.first

			self.version = similar.version + 1 if Set.new(self.artist_ids) == Set.new(similar.artist_ids)
		end
	end

	def fix_versions
		similar = []
		if self.artist_ids.empty?
			similar = Song.all_published.where(name: self.name, version: "> #{self.version}").order('version DESC')
		else
			similar = Song.all_published.where('songs.name = ? AND version > ?', self.name, self.version).order('version DESC').includes(:artists).where("artists.id IN (#{self.artist_ids.join(',')})").references(:artists)
		end

		similar.each do |s|
			s.version = s.version - 1
			s.save
		end
	end
end
