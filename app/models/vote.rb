require 'resolv'
class Vote < ActiveRecord::Base
  belongs_to :score
  belongs_to :user
  after_create :affect_score

  validates :ip, presence: true, uniqueness: { scope: :score_id }, format: { with: Resolv::IPv4::Regex }
  validates :user_id, allow_blank: true, uniqueness: { scope: :score_id }
  validates :user, presence: true, unless: 'user_id.blank?'
  validates :score, presence: true

  def change_to(u)
    return false if up == u

    if self.up
      self.score.up = self.score.up - 1
      self.up = false
      self.score.down = self.score.down + 1
    else
      self.score.down = self.score.down - 1
      self.up = true
      self.score.up = self.score.up + 1
    end

    return self.save && self.score.save
  end

  private
  def affect_score
    self.score.up = self.score.up + 1 if self.up
    self.score.down = self.score.down + 1 if !self.up
    self.score.save
  end
end