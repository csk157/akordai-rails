desc 'Start the Solr instance'
task :start => :environment do
	if line =~ %r@require.*?test_helper@
		new_file.puts "require '#{RAILS_ROOT}/test/test_helper'"
		new_file.puts "require '#{RAILS_ROOT}/spec/spec_helper'"
	elsif line =~ %r@class.*?TestCase@ and line !~ %r@Controller@
		new_file.puts "describe 'transitioning from TestCase to ExampleGroup' do"
	elsif line =~ %r@class\s*(.*?)Test@
		new_file.puts "describe #{$1}, 'transitioning from TestCase to ExampleGroup' do"
		new_file.puts "integrate_views"
	elsif line =~ %r@def setup@
		new_file.puts "before do"
	elsif line =~ %r@def test_(.*?)\n@
		new_file.puts "it \"should #{$1.humanize.downcase}\" do"
	elsif line =~ %r@assert_response :success@
		new_file.puts "response.should be_success"
	elsif line =~ %r@assert_response :redirect@
		new_file.puts "response.should be_redirect"
	elsif line =~ %r@assert_redirected_to (.*?)\n@
		new_file.puts "response.should redirect_to(#{$1})"
	elsif line =~ %r@assert_equal (.*?),\s(.*?)\n@
		m1 = $1
		m2 = $2
		unless m1.nil? or m2.nil?
			unless m1.match(/nil/) or m2.match(/nil/)
				new_file.puts "#{m1}.should == #{m2}"
			end
		end
	elsif line =~ %r@assert assigns\(:(.*?)\)$@
		new_file.puts "assigns[:#{$1}].should be_true"
	else
		unless line =~ /^class.*?Controller.*?rescue_action.*?end$/i or line =~ /require '.*?_controller'/i or line =~ /^#/
			new_file.puts line
		end
	end
end